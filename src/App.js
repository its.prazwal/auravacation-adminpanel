import React from "react";
import AppRouting from "./components/App.routing";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
//CSS
import 'antd/dist/antd.css';
import "./App.css";
import "./css/cssLoader";

function App() {
  return (
    <div>
      <ToastContainer draggable={false} autoClose={2500}/>
      <BrowserRouter>
        <AppRouting />
      </BrowserRouter>
    </div>
  );
}

export default App;

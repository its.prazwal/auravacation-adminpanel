import { toast } from "react-toastify";

export function showSuccess(msg) {
  return toast.success(msg);
}

export function showInfo(msg) {
  return toast.info(msg);
}

export function showError(msg) {
  return toast.error(msg);
}

export function showWarning(msg) {
  return toast.warn(msg);
}

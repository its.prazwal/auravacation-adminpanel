import {showError} from './Notify';

export function loginExpired(err) {
    if (err.response) {
        if (err.response.status === 440) {
            showError('Login Session Expired.')
            localStorage.clear();
            document.location.reload();
            return true;
        } else {
            return false;
        }
    }else{
        return false;
    }

}

export function invalidUser(err) {
    if (err.response) {
        if (err.response.status === 401) {
            showError('User Invalid');
            localStorage.clear();
            document.location.reload();
            return true;
        } else {
            return false;
        }
    }else{
        return false;
    }
}

export function validateLogin() {
    return !!(localStorage.getItem("uns") &&
        localStorage.getItem("fil") &&
        localStorage.getItem("aci") &&
        localStorage.getItem("id"))
}
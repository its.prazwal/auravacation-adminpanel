export function PWValidator({name, passWord, confirmPassword, value, errors, setErrors}){
    const paswd = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;
    if (name === "passWord") {
        if (!value.match(paswd)) {
            setErrors(prevState => ({ ...prevState, [name]: "Password not valid." }));
        } else if (
            confirmPassword &&
            value !== confirmPassword
        ) {
            setErrors(prevState => ({ ...prevState, [name]: "Passwords not matched",
                confirmPassword: 'Passwords not matched' }));
        } else {
            delete errors.confirmPassword;
        }
    } else if(name === 'confirmPassword') {
        if (value !== passWord)
            setErrors(prevState => ({...prevState, [name]: "Passwords not matched."}));
        else {
            if (passWord.match(paswd)) {
                delete errors.passWord;
            }
        }
    }
}

export function EmailValidator({value, errors, setErrors}){
    if (value.search(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/) !== 0) {
        setErrors(prevState => ({...prevState, emailId: 'Email Address not valid.'}))
    }else{
        delete errors.emailId;
    }
}
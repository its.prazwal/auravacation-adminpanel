import Axios from "axios";
import {validateLogin, loginExpired, invalidUser} from "./checkLogin";


function tokenAssembler() {
    if (validateLogin()) {
        const token1 = localStorage.getItem("uns");
        const token2 = localStorage.getItem("fil");
        const token3 = localStorage.getItem("aci");
        return token1 + "." + token2 + "." + token3;
    }
}

const http = Axios.create({
    baseURL: process.env.REACT_APP_BASE_URL,
    responseType: "json",
});

const usBodyHeader = {
    "Content-type": "application/json",
};

const sBodyHeader = {
    "Content-type": "application/json",
    "x-access-token": tokenAssembler(),
};

function get(url, {params = {}, responseType = "json"} = {}, secured = true) {
    return new Promise((resolve, reject) => {
        http({
            method: "GET",
            url,
            headers: secured ? sBodyHeader : usBodyHeader,
            params,
            responseType,
        })
            .then(data => resolve(data))
            .catch(err => {
                if (!loginExpired(err) && !invalidUser(err))
                    reject(err);
            })
    })
}

function post(url, {body = {}, responseType = "json"} = {}, secured = true) {
    return new Promise((resolve, reject) => {
        http({
            method: "POST",
            url,
            headers: secured ? sBodyHeader : usBodyHeader,
            data: body,
            responseType,
        })
            .then(data => resolve(data))
            .catch(err => {
                if (!loginExpired(err) && !invalidUser(err))
                    reject(err);
            })
    })
}

function put(url, {body = {}, responseType = "json"} = {}, secured = true) {
    return new Promise((resolve, reject) => {
        http({
            method: "PUT",
            url,
            headers: secured ? sBodyHeader : usBodyHeader,
            data: body,
            responseType,
        })
            .then(data => resolve(data))
            .catch(err => {
                if (!loginExpired(err) && !invalidUser(err))
                    reject(err);
            })
    })
}

function remove(
    url,
    {params = {}, body = {}, responseType = "json"} = {},
    secured = true
) {
    return new Promise((resolve, reject) => {
        http({
            method: "DELETE",
            url,
            headers: secured ? sBodyHeader : usBodyHeader,
            data: body,
            params,
            responseType,
        })
            .then(data => resolve(data))
            .catch(err => {
                if (!loginExpired(err) && !invalidUser(err))
                    reject(err);
            })
    })
}

function upload(url, method, files, data) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();
        if (files.length) {
            formData.append("img", files[0], files[0].name);
        }
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                resolve(xhr.response);
            }
        };
        xhr.open(method, url, true);
        xhr.send(formData);
    });
}

export default {
    get,
    post,
    put,
    remove,
    upload,
};

import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import Register from "./Register/Register.component";
import Dashboard from "./Dashboard/Dashboard.component";
import Login from "./Login/Login.component";
import ForgotPassword from "./Password-Options/ForgotPassword.component";
import ResetPassword from "./Password-Options/ResetPassword.component";
import Header from "./Header/Header.component";
import OptionComponent from "./Options";
import BookingIndex from "./Booking";
import DirectMailIndex from "./DirectMail";
import CustomersIndex from "./Customers";
import ReviewsIndex from "./Reviews";
import PackageIndex from "./Package";
import BookingView from "./Booking/Booking.view";
import AccountSettings from "./AccountSettings";

export default function AppRouting() {
  function NotFound() {
    return (
      <>
        <h1>Error 404!</h1>
        <h5>Page Not Found.</h5>
      </>
    );
  }

  function UnprotectedRoute({ component: Component, ...data }) {
    return (
      <>
        <Route
          {...data}
          render={(props) => {
            return (
              <div className="unprotected">
                <Component {...props} />
              </div>
            );
          }}
        />
      </>
    );
  }

  function ProtectedRoute({ component: Component, ...data }) {
    return (
      <>
        <Route
          {...data}
          render={(props) => {
            return localStorage.getItem("uns") &&
              localStorage.getItem("fil") &&
              localStorage.getItem("aci") &&
              localStorage.getItem("id") ? (
              <>
                <Header {...props} />
                <div className="content-wrapper">
                  <Component {...props} state={data.state} />
                </div>
              </>
            ) : (
              <>
                {window.localStorage.clear()}
                <Redirect to="/login"/>
              </>
            );
          }}
        />
      </>
    );
  }

  return (
    <>
      <Switch>
        <Redirect exact from="/" to="/Dashboard" />
        {/*UNPROTECTED ROUTE*/}
        <UnprotectedRoute exact path="/register" component={Register} />
        <UnprotectedRoute exact path="/login" component={Login} />
        <UnprotectedRoute
          exact
          path="/Reset-Password/:token"
          component={ResetPassword}
        />
        <UnprotectedRoute
          exact
          path="/Forgot-Password"
          component={ForgotPassword}
        />

        {/*PROTECTED ROUTES*/}
        <ProtectedRoute exact path='/Account-Settings' component={AccountSettings}/>
        <ProtectedRoute exact path='/Booking' component={BookingIndex}/>
        <ProtectedRoute exact path='/Booking/:id' component={BookingView} />
        <ProtectedRoute exact path='/Customers' component={CustomersIndex}/>
        <ProtectedRoute exact path="/Dashboard" component={Dashboard} />
        <ProtectedRoute exact path='/Direct-Mail' component={DirectMailIndex}/>
        <ProtectedRoute exact path='/Options/:optionName' component={OptionComponent}/>
        <Redirect from='/Options' to='/Options/Manage-Homepage' />
        <ProtectedRoute exact path='/Package/:which' component={PackageIndex}/>
        <ProtectedRoute exact path='/Reviews' component={ReviewsIndex}/>
        <Redirect from='/Package' to='/Package/Add-New'/>
        <ProtectedRoute component={NotFound} />
      </Switch>
    </>
  );
}

import React from "react";
import DashboardBooking from "./Dashboard.booking";
import DashboardDirectMail from "./Dashboard.directMail";
import DashboardCustDetail from "./Dashboard.CustomerDetail";
import DashboardPackage from "./Dashboard.package";
import DashboardQuickLinks from "./Dashboard.quickLinks";
import "./Dashboard.css";

function Dashboard(props) {

    return (
        <div className="dashboard-wrapper">
            <div className='dashboard-bdmr-wrapper'>
                <DashboardBooking/>
                <DashboardDirectMail props={props}/>
                <DashboardCustDetail props={props}/>
            </div>
            <DashboardPackage props={props}/>
            <DashboardQuickLinks/>
        </div>
    );
}

export default Dashboard;

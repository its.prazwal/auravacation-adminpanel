import React, {useEffect, useState} from 'react';
import httpClient from "../../utility/httpClient";
import {CUSTOMER, REVIEWS, SUBSCRIBED} from "../../constants/ApiURL.constant";

function DashboardCustDetail({props}) {
    const [details, setDetails] = useState({
        customers: [],
        reviews: [],
        subscribed: [],
    });
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        httpClient.get(CUSTOMER, {})
            .then(cust => {
                httpClient.get(SUBSCRIBED, {})
                    .then(subs => {
                        // httpClient.get(REVIEWS, {})
                        //     .then(rev => {
                                setDetails({
                                    customers: cust.data,
                                    reviews: [],
                                    subscribed: subs.data,
                                })
                         setIsLoading(false);
                        // })
                    })
            })
            .catch(err => console.trace('err: ', err));
    }, []);
    return (
        <div className='dashboard-cust-detail-wrapper'>
            <h2>Customer & Reviews:</h2>
            {isLoading ? <div className='dcdw-loader-wrapper'>
                    <div className='loader'/>
                </div> :
                <>
                    <div className='dcdw-each' onClick={()=>props.history.push('/Customers')}>
                        <h3>Customers:</h3>
                        <h4>{details.customers.length}</h4>
                    </div>
                    <div className='dcdw-each' onClick={()=>props.history.push('/Customers/Subscriber')}>
                        <h3>Subscriber:</h3>
                        <h4>{details.subscribed.length}</h4>
                    </div>
                    <div className='dcdw-each' onClick={()=>props.history.push('/Reviews')}>
                        <h3>Reviews:</h3>
                        <h4>{details.reviews.length}</h4>
                    </div>
                </>
            }
        </div>
    )
}

export default DashboardCustDetail;
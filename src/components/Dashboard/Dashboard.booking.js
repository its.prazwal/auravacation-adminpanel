import React, {useEffect, useState} from 'react';
import httpClient from "../../utility/httpClient";
import {BOOKING} from "../../constants/ApiURL.constant";
import {Link} from "react-router-dom";

function DashboardBooking() {
    const [bookingDetails, setBookingDetail] = useState({
        new: [],
        notContacted: [],
        notConfirmed: [],
    });
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        httpClient.get(BOOKING, {})
            .then(data => {
                if (data.data) {
                    const allDetail = data.data.sort((a, b) => {
                        const aDate = new Date(a.startDate);
                        const bDate = new Date(b.startDate);
                        return aDate - bDate
                    });
                    setBookingDetail({
                        new: allDetail.filter(e => e.checked === false),
                        notContacted: allDetail.filter(e => e.contacted === false),
                        notConfirmed: allDetail.filter(e => (e.contacted === true && e.confirmed === false)),
                    });
                }
                setIsLoading(false);
            })
            .catch(err => console.trace('err: ', err));
    }, []);
    return (
        <div className='dashboard-booking-wrapper'>
            <h2>Booking Details</h2>
            {isLoading ? <div className='dbw-loader-wrapper'>
                    <div className='loader'/>
                </div> :
                <>
                    <div className='dbw-each'>
                        <h3>New: <b>{bookingDetails.new.length}</b></h3>
                        {bookingDetails.new.map(nbd => {
                            return (<h4 key={nbd._id}><Link to={`Booking/${nbd._id}`}>
                                {new Date(nbd.startDate).toDateString()} - {nbd.pax} pax</Link></h4>)
                        })}
                    </div>
                    <div className='dbw-each'>
                        <h3>Not Contacted: <b>{bookingDetails.notContacted.length}</b></h3>
                        {bookingDetails.notContacted.map(ncbd => {
                            return (<h4 key={ncbd._id}><Link to={`Booking/${ncbd._id}`}>
                                {new Date(ncbd.startDate).toDateString()} - {ncbd.pax} pax</Link></h4>)
                        })}
                    </div>
                    <div className='dbw-each'>
                        <h3>Contacted but not confirmed: <b>{bookingDetails.notConfirmed.length}</b></h3>
                        {bookingDetails.notConfirmed.map(ncbd => {
                            return (<h4 key={ncbd._id}><Link to={`Booking/${ncbd._id}`}>
                                {new Date(ncbd.startDate).toDateString()} - {ncbd.pax} pax</Link></h4>)
                        })}
                    </div>
                </>
            }
        </div>
    )
}

export default DashboardBooking;
import React, {useEffect, useState} from 'react';
import httpClient from "../../utility/httpClient";
import {PACKAGE, TRIP_FACTS} from "../../constants/ApiURL.constant";

function DashboardPackage({props}) {
    const [allPackages, setAllPackages] = useState([]);
    const [styles, setStyles] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        httpClient.get(TRIP_FACTS.STYLES, {})
            .then(data => {
                setStyles(data.data);
                httpClient.get(PACKAGE.COMMON, {})
                    .then(data => {
                        setAllPackages(data.data);
                            setIsLoading(false);
                    })
            })
    }, [])
    return (

        <div className='dashboard-package-wrapper'>
            <h2>Package Details</h2>
            <div className='dpw-each'
                 onClick={() => props.history.push('/Package/Add-New')}>
                <h4>Add <br/> New</h4>
                <h3>+</h3>
            </div>
            {isLoading ?
                <div className='dpw-loader-wrapper'>
                    <div className='loader'/>
                </div> : styles.length > 0 ?
                    styles.map(sty => {
                        const count = allPackages.filter(pack => pack.style === sty.name).length;
                        const textToUrl = sty.name.replace(/\s/g, "-");
                        return (
                            <div className='dpw-each' key={sty._id}
                                 onClick={() => props.history.push(`/Package/${textToUrl}`)}>
                                <h4>{sty.name}</h4>
                                <h3>{count}</h3>
                            </div>
                        )
                    }) : <div className='No-Data'/>}
        </div>

    )
}

export default DashboardPackage;
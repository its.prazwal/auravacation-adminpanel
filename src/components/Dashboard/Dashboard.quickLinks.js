import React from 'react';
import {Link} from "react-router-dom";

function DashboardQuickLinks() {
    const links = ['Manage-Homepage', 'Location', 'Cost-Details', 'Trip-Facts', 'Adventure-Activities', 'Company-Profile']
    return (
        <div className='dashboard-quickLinks-wrapper'>
            <h2>Quick Links</h2>
            {links.map((l, i) => {
                return (
                    <Link to={`/Options/${l}`} key={i}>{l.toUpperCase()}</Link>
                )
            })}
        </div>
    )
}

export default DashboardQuickLinks;
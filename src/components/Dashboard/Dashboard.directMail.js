import React, {useEffect, useState} from 'react';
import httpClient from "../../utility/httpClient";
import {DIRECT_MAIL} from "../../constants/ApiURL.constant";

function DashboardDirectMail({props}) {
    const [directMails, setDirectMails] = useState({
        notRead: [],
        notReplied: [],
    });
    const [isLoading, setIsLoading] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        httpClient.get(DIRECT_MAIL, {})
            .then(data => {
                if (data.data) {
                    const allMails = data.data.sort((a, b) => {
                        const aDate = new Date(a.receivedDate);
                        const bDate = new Date(b.receivedDate);
                        return aDate - bDate
                    });
                    setDirectMails({
                        notRead: allMails.filter(e => e.read === false),
                        notReplied: allMails.filter(e => (e.replied === false && e.read === true)),
                    });
                }
                 setIsLoading(false);
            })
            .catch(err => console.trace('err: ', err));
    }, []);
    return (
        <div className='dashboard-directMail-wrapper' onClick={()=>props.history.push('/Direct-Mail')}>
            <h2>Enquiry Details</h2>
            {isLoading ? <div className='ddmw-loader-wrapper'>
                    <div className='loader'/>
                </div> :
                <>
                    <div className='ddmw-each'>
                        <h3>Not Read: <b>{directMails.notRead.length}</b></h3>
                        {directMails.notRead.map(ndm => {
                            return (<h4 key={ndm._id}>
                                {new Date(ndm.receivedDate).toDateString()} - {ndm.customerId}</h4>)
                        })}
                    </div>
                    <div className='ddmw-each'>
                        <h3>Read but not Replied: <b>{directMails.notReplied.length}</b></h3>
                        {directMails.notReplied.map(nrdm => {
                            return (<h4 key={nrdm._id}>
                                {new Date(nrdm.receivedDate).toDateString()} - {nrdm.customerId}</h4>)
                        })}
                    </div>
                </>
            }
        </div>
    )
}

export default DashboardDirectMail;
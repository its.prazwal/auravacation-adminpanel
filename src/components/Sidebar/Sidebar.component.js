import React from "react";
import { NavLink } from "react-router-dom";

import "./Sidebar.css";

function SideBar({ navs, mainNav }) {
  return navs.length > 0 ? (
    <div className="sidebar-wrapper">
      <NavLink
    exact
    to={`/${mainNav}/`}
    activeClassName="sidebar-element-active"
    />
      {navs.map((e, i) => {
        const _name = e.replace(/\s/g, "-");
        return (
          <NavLink
            to={`/${mainNav}/${_name}`}
            key={i}
            activeClassName="sidebar-element-active"
          >
            <button className="sidebar-element">{e}</button>
          </NavLink>
        );
      })}
    </div>
  ) : null;
}

export default SideBar;

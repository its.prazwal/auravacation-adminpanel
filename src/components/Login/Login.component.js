import React, {useEffect, useRef, useState} from "react";
import { Link } from "react-router-dom";
import LoginForm from "./Login.form";
import httpClient from "./../../utility/httpClient";
import * as notify from "../../utility/Notify";
import {USER} from "../../constants/ApiURL.constant";

function Login(props) {
  const [loginData, setLoginData] = useState({
    userName: "",
    passWord: "",
  });
  const [errors, setErrors] = useState({});
  const userNameRef = useRef(null);

  function handleChange(e) {
    const { name, value } = e.target;
    const updatedValue = { ...loginData, [name]: value };
    setLoginData(updatedValue);
    delete errors[name];
  }

  useEffect(()=>{
    httpClient
        .get(USER.REGISTER, {}, false)
        .then((data) => {
            props.history.push('/register');
            notify.showInfo('Please register new user.')
        })
        .catch((err) => {
            console.log('err: ',err);
            notify.showInfo('Please Login')
        });
  },[props.history])

  function handleSubmit(e) {
    e.preventDefault();
    if (validation()) {
      httpClient
        .post("/user/login", { body: loginData }, false)
        .then((data) => {
          const tarr = data.data.token.split(".");
          localStorage.setItem("fil", tarr[1]);
          localStorage.setItem("uns", tarr[0]);
          localStorage.setItem("aci", tarr[2]);
          localStorage.setItem("id", data.data.id);
          props.history.push("/Dashboard");
          document.location.reload();
        })
        .catch((err) => {
          notify.showError("Login Failure.");
          setLoginData({
            userName: "",
            passWord: "",
          });
          setErrors({
            userName: "Incorrect Username Or Password",
            passWord: "Incorrect Username Or Password",
          });
          userNameRef.current.focus();
        });
    } else {
      userNameRef.current.focus();
    }
  }

  function validation() {
    let _errors = {};
    if (!loginData.userName) _errors.userName = "Username is required";
    if (!loginData.passWord) _errors.passWord = "Password is required";
    setErrors(_errors);
    return Object.keys(_errors).length === 0;
  }
  return (
    <div className="auth-page login-page">
      <h1>Admin Panel</h1>
      <h5>Please enter your login credentials</h5>
      <LoginForm
        errors={errors}
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        value={loginData}
        refs={{userNameRef}}
      />
      <br />
      <Link to="/Forgot-Password">Forgot Password?</Link>
    </div>
  );
}

export default Login;

import React from "react";
import {Form, Input} from 'antd';
import {UserOutlined, LockOutlined} from '@ant-design/icons';

function LoginForm(props) {
  const { errors, value, handleChange, handleSubmit, refs } = props;

  const SubmitButton = (
    <button
      type="submit"
      className={`button button-${
        Object.keys(errors).length > 0 ? "disabled" : 'primary'
      }`}
      {...Object.assign(
        {},
        Object.keys(errors).length > 0 ? { disabled: true } : null
      )}
        onClick={handleSubmit}
    >
      Login
    </button>
  );
  return (
    <form onSubmit={handleSubmit} className="form-wrapper">
        <Form.Item
            validateStatus={ errors.userName && 'error' }
            hasFeedback
            help={ errors.userName || null}
        >
            <Input
                ref={refs.userNameRef}
                id="userName"
                autoFocus={true}
                name="userName"
                onChange={handleChange}
                placeholder="Enter your username"
                prefix={<UserOutlined className="site-form-item-icon" />}
                value={value.userName}
            />
        </Form.Item>
        <Form.Item
            validateStatus={ errors.passWord &&  'error' }
            hasFeedback
            help={ errors.passWord || null}
        >
            <Input.Password
                name="passWord"
                onChange={handleChange}
                placeholder="Password"
                prefix={<LockOutlined className="site-form-item-icon"/>}
                value={value.passWord}
            />
        </Form.Item>
      {SubmitButton}
    </form>
  );
}

export default LoginForm;

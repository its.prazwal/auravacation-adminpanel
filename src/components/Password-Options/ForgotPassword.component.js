import React, { useState } from "react";
import * as notify from "../../utility/Notify";
import httpClient from "./../../utility/httpClient";
import { Link } from "react-router-dom";
import {Form, Input} from "antd";
import {MailOutlined} from '@ant-design/icons';
import {EmailValidator} from "../../utility/Validator";

function ForgotPassword(props) {
  const [emailId, setEmailId] = useState("");
  const [errors, setErrors] = useState({});
  const [onTyping, setOnTyping] = useState('');

  function handleChange(e) {
    const { name, value } = e.target;
    setEmailId(value);
    EmailValidator({value, errors, setErrors})
    delete errors[name];
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (validation()) {
      httpClient
        .post("/user/forgot-password", { body: { emailId } })
        .then((data) => {
          notify.showSuccess("Reset Link Send to Email Address.");
          setTimeout(() => {
            props.history.push("/Auth/Login");
          }, 2000);
        })
        .catch((err) => {
          setErrors({ emailId: "Email not registered." });
          notify.showError('Email address not registered.');
        });
    } else {
      document.getElementById("emailId").focus();
    }
  }

  function validation() {
    let _errors = {};
    if (!emailId)
      _errors.emailId = "Email Address is Required.";
    setErrors(_errors);
    return Object.keys(_errors).length === 0;
  }
  return (
    <div className="auth-page forgot-password">
      <h1>Forgot Password</h1>
      <h5>Please enter registered Email Address.</h5>
      <Form className="form-wrapper" onSubmit={handleSubmit}>
        <Form.Item
        validateStatus={
        onTyping === 'emailId' ? 'validating' :
            errors.emailId ? 'error' : emailId && 'success'}
        hasFeedback
        help={onTyping === 'emailId' ? null : errors.emailId || null}
        >
        <Input
            autoFocus={true}
            id='emailId'
            name="emailId"
            onChange={handleChange}
            placeholder="Enter your Email ID"
            prefix={<MailOutlined className="site-form-item-icon"/>}
            value={emailId}
            onInput={()=> {
              setOnTyping('emailId');
              setTimeout(function(){
                setOnTyping('')
              },  2200)
            }}
        />
      </Form.Item>
      <button
          type="submit"
          className={`button button-${
            Object.keys(errors).length === 0 ? "primary" : "disabled"
          }`}
          {...Object.assign(
            {},
            Object.keys(errors).length === 0 ? null : { disabled: true }
          )}
          name="submit"
          onClick={handleSubmit}
        >
          Submit
        </button>
      </Form>
      <br />
      <Link to="/Auth/Login">Back To Login</Link>
    </div>
  );
}

export default ForgotPassword;

import React, { useState } from "react";
import * as notify from "../../utility/Notify";
import httpClient from "./../../utility/httpClient";
import { Link } from "react-router-dom";
import ResetPasswordForm from "./ResetPassword.form";
import {PWValidator} from "../../utility/Validator";
import {USER} from "../../constants/ApiURL.constant";

function ResetPassword(props) {
  const [passwords, setPasswords] = useState({
    passWord: "",
    confirmPassword: "",
  });
  const [errors, setErrors] = useState({});

  const _token = props.match.params.token;

  function handleChange(e) {
    const { name, value } = e.target;
    setPasswords({ ...passwords, [name]: value });
    PWValidator({
      name,
      passWord:passwords.passWord,
      confirmPassword: passwords.confirmPassword,
      value:value,
      errors,
      setErrors,
    })
    delete errors[name];
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (validation()) {
      httpClient
        .put(`${USER.RESET_PASSWORD}/${_token}`, {
          body: { passWord: passwords.passWord },
        })
        .then((data) => {
          notify.showSuccess(data.data);
        })
        .catch((err) => {
          notify.showError(err.response.data.msg);
        });
      setTimeout(() => {
        notify.showInfo("Please Login to continue.");
        props.history.push("/Login");
      }, 2500);
    } else {
      document.getElementById("passWord").focus();
    }
  }

  function validation() {
    let _errors = {};
    if (!passwords.passWord)
      _errors.passWord = "New Password is required.";
    if (!passwords.confirmPassword)
      _errors.confirmPassword = "Please write the password again.";
    setErrors(_errors);
    return Object.keys(_errors).length === 0;
  }

  return (
    <div className="auth-page reset-password">
      <h2>RESET PASSWORD</h2>
      <ResetPasswordForm
        handleChange={handleChange}
        value={passwords}
        errors={errors}
        handleSubmit={handleSubmit}
      />
      <br />
      <Link to="/Auth/Login">Back to Login</Link>
    </div>
  );
}

export default ResetPassword;

import React, {useState} from "react";
import {Form, Input} from 'antd';
import {LockOutlined} from '@ant-design/icons';

function ResetPasswordForm({
  value,
  errors,
  handleSubmit,
    handleChange,
}) {
  const [onTyping, setOnTyping] = useState('');

  return (
    <Form className="form-wrapper" onSubmit={handleSubmit}>
      <h6>
        Please input password of 8 to 15 characters containing at least 1 of
        each: lowercase, uppercase, numeric digit and special character.
      </h6>
      <Form.Item
          validateStatus={
            onTyping === 'passWord' ? 'validating' :
                errors.passWord ? 'error' : value.passWord && 'success'}
          hasFeedback
          help={onTyping === 'passWord' ? null: errors.passWord || null}
      >
        <Input.Password
            autoFocus={true}
            id="passWord"
            name="passWord"
            onChange={handleChange}
            placeholder="New Password"
            prefix={<LockOutlined className="site-form-item-icon"/>}
            value={value.passWord}
            onInput={()=> {
              setOnTyping('passWord');
              setTimeout(function(){
                setOnTyping('')
              },  2200)
            }}
        />
      </Form.Item>
      {value.passWord ?
          <Form.Item
              validateStatus={
                onTyping === 'confirmPassword' ? 'validating' :
                    errors.confirmPassword ? 'error' : value.confirmPassword && 'success'}
              hasFeedback
              help={ onTyping === 'confirmPassword' ? null : errors.confirmPassword || null}
          >
            <Input.Password
                name="confirmPassword"
                onChange={handleChange}
                placeholder="Confirm Password"
                prefix={<LockOutlined className="site-form-item-icon"/>}
                value={value.confirmPassword}
                onInput={()=> {
                  setOnTyping('confirmPassword');
                  setTimeout(function(){
                    setOnTyping('')
                  },  2200)
                }}
            />
          </Form.Item>
          : null}
      <button
        type="submit"
        name="submit"
        className={`button button-${
          Object.keys(errors).length === 0 ? "primary" : "disabled"
        }`}
        {...Object.assign(
          {},
          Object.keys(errors).length === 0 ? null : { disabled: true }
        )}
          onClick={handleSubmit}
      >
        Submit
      </button>
    </Form>
  );
}

export default ResetPasswordForm;

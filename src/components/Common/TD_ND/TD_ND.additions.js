import React, {useEffect, useState} from "react";
import {Modal, Form, Input, Button} from "antd";
import TableInput from "../TableInput";
import httpClient from "../../../utility/httpClient";

const {TextArea} = Input
const initialTaskState = {
    isEditing: {index: -1, value: ''},
}

function AdditionalModal({additionalData, visible, hideMAModal, httpURL}) {
    const [newSt, setNewSt] = useState('');
    const [dataInModal, setDataInModal] = useState({});
    const [taskState, setTaskState] = useState(initialTaskState);
    const [submitting, setSubmitting] = useState(false);

    useEffect(() => {
        setDataInModal(additionalData);
        setSubmitting(false);
    }, [visible, additionalData])

    function closeModal() {
        setNewSt('');
        setTaskState(initialTaskState);
        hideMAModal();
    }

    function handleChange(e) {
        const {name, value} = e.target;
        if (name === 'newSt')
            setNewSt(value);
        if (name === 'editingSt')
            setTaskState(prevState => ({...prevState, isEditing: {...prevState.isEditing, value}}))
        if (name === 'otherInfo')
            setDataInModal(prevState => ({...prevState, otherInfo: value}));
    }

    function handleAddNew(e) {
        e.preventDefault();
        let arrayUpdater = [...dataInModal.majorAttractions];
        arrayUpdater.push(newSt)
        setDataInModal(prevState => ({...prevState, majorAttractions: arrayUpdater}));
        setNewSt('')
    }

    function handleEdit(e) {
        const {name, value} = e.target;
        switch (name) {
            case 'editSt':
                const _index = parseInt(value);
                setTaskState(prevState => ({
                    ...prevState, isEditing: {
                        index: _index,
                        value: dataInModal.majorAttractions.filter((e, i) => i === _index)[0],
                    }
                }))
                break;
            case 'cancelEdit':
                setTaskState(prevState => ({...prevState, isEditing: initialTaskState.isEditing}))
                break;
            case 'saveEdited':
                let arrayUpdater = [...dataInModal.majorAttractions];
                arrayUpdater.splice(taskState.isEditing.index, 1, taskState.isEditing.value);
                setDataInModal(prevState => ({...prevState, majorAttractions: arrayUpdater}));
                setTaskState(prevState => ({...prevState, isEditing: initialTaskState.isEditing}))
                break;
            case 'removeSt':
                let arrayUpdater2 = [...dataInModal.majorAttractions];
                arrayUpdater2.splice(parseInt(value), 1);
                setDataInModal(prevState => ({...prevState, majorAttractions: arrayUpdater2}));
                break;
            default:
                break;
        }
        e.preventDefault();
    }

    function saveChanges() {
        setSubmitting(true);
        httpClient.put(httpURL, {body: dataInModal})
            .then(data => {
                setSubmitting(false);
                closeModal();
            })
            .catch(err => console.trace('err: ', err))
    }

    return (
        <>
            {dataInModal &&
            <Modal
                className='TD_ND-additions-modal'
                title={`${visible.MA ? 'Major Attractions': visible.OI ? "Other Information" : ''}
                 of ${dataInModal.title || dataInModal.name}`}
                visible={visible.MA || visible.OI}
                onCancel={closeModal}
                centered
                footer={
                    <Button loading={submitting} className='button button-primary' onClick={saveChanges}>Done</Button>
                }
                width="800px"
                bodyStyle={{height: '350px'}}
            >
                {visible.MA &&
                <>
                    <Form className='TDND-majorAttractions' onSubmitCapture={handleAddNew}>
                        <Input
                            name='newSt'
                            placeholder='Statement of MAJOR ATTRACTIONS'
                            value={newSt}
                            onChange={handleChange}
                        />
                        <button
                            className={`button button-${newSt ? "primary" : "disabled"}`}
                            name="addNew"
                            {...Object.assign({}, newSt ? null : {disabled: true})}
                            type="submit"
                        >
                            Add
                        </button>
                    </Form>
                    <TableInput
                        editing={taskState.isEditing}
                        handleSubmit={handleEdit}
                        tableData={dataInModal.majorAttractions}
                        handleChange={handleChange}
                    /></>}

                {visible.OI &&
                <TextArea
                    placeholder='Other Information...'
                    value={dataInModal.otherInfo}
                    name='otherInfo'
                    autoFocus={true}
                    onChange={handleChange}
                    allowClear={true}
                />
                }
            </Modal>
            }
        </>
    )
}

export default AdditionalModal;
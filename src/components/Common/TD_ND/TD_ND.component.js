import React, {useEffect, useRef, useState} from 'react';
import {Form, Input} from 'antd';
import * as APIURL from '../../../constants/ApiURL.constant';
import * as notify from '../../../utility/Notify';
import './TD_ND.css';
import httpClient from "../../../utility/httpClient";
import AdditionalModal from "./TD_ND.additions";

const {TextArea} = Input;
const initialTaskState = {
    isAddNew: true,
    isEditing: '',
    viewData: {},
}

const initialTDFormState = {title: '', description: ''}
const initialNDFormState = {name: '', detail: ''}

function TDNDComponent({head, subHead, TDorND, additions}) {
    subHead = subHead.replace(/\s+/g,'_');
    let initialFormState = {new: TDorND === 'TD' ? {...initialTDFormState} : {...initialNDFormState}, old: {}}
    const newInputRef = useRef(null);
    const [taskState, setTaskState] = useState(initialTaskState);
    const [formState, setFormState] = useState(initialFormState);
    const [dbData, setDbData] = useState([]);
    const [visibleState, setVisible] = useState({MA: false, OI: false})
    const [additionalMD, setAdditionalMD] = useState({});

    useEffect(() => {
        refreshData();
        setTaskState(initialTaskState);
        document.getElementById('TDND-addNew-form').style.height = '0px';
    }, [head, subHead, TDorND])

    function refreshData() {
        httpClient.get(APIURL[head][subHead], {})
            .then(data => setDbData(data.data))
            .catch((err => {
                notify.showError('Error Loading!!!');
                console.trace('err: ', err);
            }))
    }

    function showOrHideForm() {
        document.getElementById('TDND-addNew-form').style.height = taskState.isAddNew ? '350px' : '0px';
        setTaskState(prevState => ({...prevState, isAddNew: !taskState.isAddNew}))
        setFormState(initialFormState);
        newInputRef.current.focus();
    }

    function handleChange(e) {
        const {id, name, value} = e.target;
        if (id === 'newT' || id === 'newD')
            setFormState(prevState => ({...prevState, new: {...prevState.new, [name]: value}}));
        else
            setFormState(prevState => ({...prevState, old: {...prevState.old, [name]: value}}));
    }

    function handleAddNew(e) {
        e.preventDefault();
        httpClient.post(APIURL[head][subHead], {body: formState.new})
            .then(data => {
                showOrHideForm();
                refreshData();
            })
            .catch(err => console.trace('err: ', err));
    }

    function handleEachData(e) {
        const {name, value} = e.target;
        switch (name) {
            case 'edit':
                setTaskState(prevState => ({...prevState, isEditing: value}));
                setFormState(prevState => ({...prevState, old: dbData.filter(dbd => dbd._id === value)[0]}))
                hideView();
                setTimeout(() => {
                    document.getElementById('oldT').focus()
                }, 1)
                break;
            case 'cancel':
                setTaskState(prevState => ({...prevState, isEditing: ''}))
                setFormState(prevState => ({...prevState, old: {}}))
                break;
            case 'view':
                setTaskState(prevState => ({...prevState, viewData: dbData.filter(dbd => dbd._id === value)[0]}))
                document.getElementById("ED-view-wrapper").style.display = "block";
                break;
            case 'delete':
                httpClient.remove(APIURL[head][subHead], {body: {_id: value}})
                    .then(data => {
                        refreshData();
                    })
                    .catch(err => console.trace('err: ', err));
                break;
            case 'save':
                httpClient.put(APIURL[head][subHead], {body: formState.old})
                    .then(data => {
                        refreshData();
                        setTaskState(prevState => ({...prevState, isEditing: ''}))
                        setFormState(prevState => ({...prevState, old: {}}))
                    })
                    .catch(err => console.trace('err: ', err));
                break;
            default:
                break;
        }

    }

    function hideView(e) {
        setTaskState(prevState => ({...prevState, viewData: {}}))
        document.getElementById("ED-view-wrapper").style.display = "none";
    }

    function showMAModal(e) {
        const {name, value} = e.target;
        setVisible(prevState => ({...prevState, [name]: true}))
        setAdditionalMD(dbData.filter((e) => e._id === value)[0]);
    }

    function hideMAModal() {
        setVisible({MA: false, OI: false});
        refreshData();
    }

    const addButton = <button
        className={`button button-${taskState.isAddNew ? 'primary' : 'cancel'}`}
        onClick={showOrHideForm}
    >
        {!taskState.isAddNew ? 'Cancel' : 'Add New'}
    </button>

    const eachDataButtons = (ed) => {
        return (
            <>
                <button
                    name={taskState.isEditing === ed._id ? 'cancel' : 'delete'}
                    className={`button button-${taskState.isEditing === ed._id ? 'cancel' : 'remove'}`}
                    value={ed._id}
                    onClick={handleEachData}
                >
                    {taskState.isEditing === ed._id ? 'Cancel' : 'Delete'}
                </button>
                <button
                    name={taskState.isEditing === ed._id ? 'save' : 'edit'}
                    className={`button button-${taskState.isEditing === ed._id ? 'primary' : 'secondary'}`}
                    value={ed._id}
                    onClick={handleEachData}
                >
                    {taskState.isEditing === ed._id ? 'Save' : 'Edit'}
                </button>
                {taskState.isEditing !== ed._id &&
                <button
                    className={`button button-primary`}
                    value={ed._id}
                    onClick={handleEachData}
                    name='view'
                >
                    View
                </button>
                }
            </>
        )
    }
    const eachDataForm = <>
        <Input
            id='oldT'
            allowClear
            name={TDorND === 'TD' ? 'title' : 'name'}
            placeholder={`${TDorND === 'TD' ? 'Title' : 'Name'} of ${subHead.replace(/_/g,' ')}`}
            value={formState.old.title || formState.old.name}
            onChange={handleChange}
        />
        <TextArea
            id='oldD'
            allowClear
            name={TDorND === 'TD' ? 'description' : 'detail'}
            placeholder={`${TDorND === 'TD' ? 'Description' : 'Detail'} of ${subHead.replace(/_/g,' ')}`}
            onChange={handleChange}
            value={formState.old.description || formState.old.detail}
        />
    </>
    const eachDataAdditionButtons = (ed) => {
        return (
            <>
                {additions.map((ele, i) => {
                    return (
                        <button
                            name={ele}
                            className='button button-secondary'
                            value={ed._id}
                            onClick={showMAModal}
                            key={i}
                        >
                            {ele === 'MA' && 'Major Attractions'}
                            {ele === 'OI' && 'Other Information'}
                            {ele === 'VS' && 'Visit Seasons'}
                        </button>
                    )
                })}
            </>
        )
    }
    const viewDataDiv = <div className="modal-box" id="ED-view-wrapper">
        <div className="ED-view-data" id='ED-view-data'>
          <span className="modal-close" onClick={hideView}>
            &times;
          </span>
            <button
                className="button button-primary"
                type="button"
                value={taskState.viewData._id}
                name='edit'
                onClick={handleEachData}
            >
                Edit
            </button>
            <h2>{taskState.viewData.name || taskState.viewData.title}</h2>
            <p>{taskState.viewData.description || taskState.viewData.detail}</p>
        </div>
    </div>

    return (
        <>
            {additions &&
            <AdditionalModal
                visible={visibleState}
                hideMAModal={hideMAModal}
                httpURL={APIURL[head][subHead]}
                additionalData={additionalMD}
            />}
            {viewDataDiv}
            <div className='TDND-wrapper'>
                {addButton}
                <Form id='TDND-addNew-form'>
                    <Input
                        id='newT'
                        ref={newInputRef}
                        allowClear
                        name={TDorND === 'TD' ? 'title' : 'name'}
                        placeholder={`${TDorND === 'TD' ? 'Title' : 'Name'} of ${subHead.replace(/_/g,' ')}`}
                        value={formState.new.title || formState.new.name}
                        onChange={handleChange}
                    />
                    <TextArea
                        id='newD'
                        allowClear
                        name={TDorND === 'TD' ? 'description' : 'detail'}
                        placeholder={`${TDorND === 'TD' ? 'Description' : 'Detail'} of ${subHead.replace(/_/g,' ')}`}
                        onChange={handleChange}
                        value={formState.new.description || formState.new.detail}
                    />
                    {(formState.new.title || formState.new.name) &&
                    <button className="button button-primary" onClick={handleAddNew}>
                        Add
                    </button>
                    }
                </Form>
                <div className='TDND-data-wrapper'>
                    {dbData.length === 0 ?
                        <div className='No-Data'/>
                        : dbData.map((ed, i) => {
                            return (
                                <div className='TDND-eachData' key={i}>
                                    {eachDataButtons(ed)}
                                    {taskState.isEditing === ed._id ? eachDataForm :
                                        <>
                                            <h4>{ed.title || ed.name}</h4>
                                            <p>{ed.description || ed.detail}</p>
                                            {additions &&
                                            <div className='TDND-eachData-lwrBtn'>
                                                {eachDataAdditionButtons(ed)}
                                            </div>
                                            }
                                        </>
                                    }
                                </div>
                            )
                        })}
                </div>
            </div>
        </>
    )
}

export default TDNDComponent;
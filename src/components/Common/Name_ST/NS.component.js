import React, {useEffect, useState} from "react";
import {Form, Input} from "antd";
import './NS.css';
import TableInput from "../TableInput";
import httpClient from "../../../utility/httpClient";
import * as APIURL from '../../../constants/ApiURL.constant';

// tableData, editing, handleSubmit, error, handleChange

const initialEditingState = {index: -1, value: ''};

function NameStatement({NorS, head, subHead}) {
    subHead = subHead.replace(/\s+/g, '_');
    const [newSt, setNewSt] = useState('');
    const [dbData, setDBData] = useState([]);
    const [editing, setEditing] = useState(initialEditingState)

    useEffect(() => {
        refreshData();
    }, [head, subHead]);

    function refreshData() {
        httpClient.get(APIURL[head][subHead], {})
            .then(data => setDBData(data.data))
            .catch(err => console.trace('err: ', err));
    }

    function handleChange(e) {
        const {name, value} = e.target;
        if (name === 'newSt')
            setNewSt(value)
        if (name === 'editingSt')
            setEditing(prevState => ({...prevState, value}));
    }

    function handleAddNew(e) {
        httpClient.post(APIURL[head][subHead], {body: {[NorS]: newSt}})
            .then(data => {
                refreshData();
                setNewSt('');
            })
            .catch(err => console.trace('err: ', err));
        e.preventDefault();
    }

    function handleEdit(e) {
        const {name, value} = e.target;
        switch (name) {
            case 'editSt':
                setEditing({
                    index: value,
                    value: dbData.filter(e => e._id === value)[0][NorS],
                })
                break;
            case 'cancelEdit':
                setEditing(initialEditingState);
                break;
            case 'saveEdited':
                httpClient.put(APIURL[head][subHead], {body: {_id: editing.index, [NorS]: editing.value}})
                    .then(data => {
                        refreshData();
                        setEditing(initialEditingState);
                    })
                    .catch(err => console.trace('err: ', err));
                break;
            case 'removeSt':
                httpClient.remove(APIURL[head][subHead], {body: {_id: value}})
                    .then(data => refreshData())
                    .catch(err => console.trace('err: ', err));
                break;
            default:
                break;
        }
        e.preventDefault();
    }

    return (
        <>
            <Form className='NS-form' onSubmitCapture={handleAddNew}>
                <Input
                    name='newSt'
                    placeholder={`${NorS} of ${subHead.replace('_', ' ')}...`}
                    value={newSt}
                    onChange={handleChange}
                    autoFocus={true}
                />
                <button
                    className={`button button-${newSt ? "primary" : "disabled"}`}
                    name="addNew"
                    {...Object.assign({}, newSt ? null : {disabled: true})}
                    type="submit"
                    onClick={handleAddNew}
                >
                    Add
                </button>
            </Form>
            <TableInput
                handleChange={handleChange}
                tableData={dbData}
                handleSubmit={handleEdit}
                editing={editing}
                NorS={NorS}
            />
        </>
    )
}

export default NameStatement;
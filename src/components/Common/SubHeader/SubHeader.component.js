import React from "react";
import './SubHeader.css';

function SubHeaderBar({subHeads, chosen, handleChoose}) {
    return (
        <>
            {subHeads.length > 0 &&
            <div className="subHeader-wrapper">
                {subHeads.map((e, i) => {
                    return (
                        <button
                            type="button"
                            key={i}
                            className={
                                chosen === e ? "subH-btn-active" : "subH-btn-inactive"
                            }
                            value={e}
                            onClick={handleChoose}
                            style={{width: `${100/subHeads.length}%`}}
                        >
                            {e.toUpperCase()}
                        </button>
                    );
                })}
            </div>}
        </>
    );
}

export default SubHeaderBar;

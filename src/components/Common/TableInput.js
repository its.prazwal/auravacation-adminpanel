import React from 'react';
import {Input} from 'antd';

function TableInput({tableData, editing, handleSubmit, handleChange, NorS}) {

    return (
        <div className='TDND-table-wrapper'>
            <table className="TDND-table">
                <thead>
                <tr>
                    <th> -</th>
                    <th>
                        {NorS === 'N' ? 'Name' : 'Statement'}
                        {editing.index !== -1 ? <i>editing {NorS === 'N' ? 'Name' : 'Statement'}...</i> : null}
                    </th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {tableData.length > 0 ?
                    tableData.map((ele, i) => {
                        const isObject = typeof ele === 'object';
                        return (
                            <tr key={i}>
                                <td>
                                    <button value={isObject ? ele._id : i} onClick={handleSubmit}
                                            name='removeSt' className='button button-closeIcon'>
                                    </button>
                                </td>
                                <td>
                                    {editing.index === (isObject ? ele._id : i) ? (
                                        <form onSubmit={handleSubmit} name="saveEdited">
                                            <Input
                                                id='old'
                                                placeholder='Editing Statement.'
                                                value={editing.value}
                                                name='editingSt'
                                                autoFocus={true}
                                                onChange={handleChange}
                                            />
                                        </form>
                                    ) : (
                                        isObject ? (ele.name || ele.statement) : ele
                                    )}
                                </td>
                                <td>
                                    <button
                                        type={editing.index === (isObject ? ele._id : i) ? "submit" : "button"}
                                        className={`button button-${editing.index > -1 &&
                                        editing.index !== (isObject ? ele._id : i)
                                            ? 'disabled-2'
                                            : 'secondary'}`}
                                        name={editing.index === (isObject ? ele._id : i) ? "saveEdited" : "editSt"}
                                        onClick={handleSubmit}
                                        value={isObject ? ele._id : i}
                                        {...Object.assign({}, editing.index > -1 &&
                                        editing.index !== (isObject ? ele._id : i)
                                            ? {disabled: true}
                                            : null)}
                                    >
                                        {editing.index === (isObject ? ele._id : i) ? "Save" : "Edit"}
                                    </button>
                                    {editing.index === (isObject ? ele._id : i) &&
                                    <button
                                        className="button button-remove"
                                        name="cancelEdit"
                                        onClick={handleSubmit}
                                        type="button"
                                        value={isObject ? ele._id : i}
                                    >
                                        Cancel
                                    </button>
                                    }
                                </td>
                            </tr>
                        );
                    }) :
                    <tr>
                        <td colSpan={3}>
                            <div className='No-Data'/>
                        </td>
                    </tr>
                }
                </tbody>
            </table>
        </div>
    );
}

export default TableInput;

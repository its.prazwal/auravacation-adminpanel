import React, {useState} from "react";
import {Upload} from 'antd';
import {LoadingOutlined, PlusOutlined} from '@ant-design/icons';
import ImgCrop from 'antd-img-crop';
import {validateLogin} from "../../utility/checkLogin";
import {showError} from '../../utility/Notify';

function tokenAssembler() {
    if (validateLogin()) {
        const token1 = localStorage.getItem("uns");
        const token2 = localStorage.getItem("fil");
        const token3 = localStorage.getItem("aci");
        return token1 + "." + token2 + "." + token3;
    }
}

export function ImageUploader({url, method, saveImgUrl, cropImage = false}) {
    const [imageUrl, setImageUrl] = useState(saveImgUrl);
    const [loading, setLoading] = useState(false)
    url = `${process.env.REACT_APP_BASE_URL}${url}`

    function getBase64(img, callback) {
        const reader = new FileReader();
        reader.addEventListener('load', () => callback(reader.result));
        reader.readAsDataURL(img);
    }

    function onChange(info) {
        if (info.file.status === 'uploading') {
            setLoading(true);
            return;
        }
        if (info.file.status === 'done') {
            getBase64(info.file.originFileObj, url => setImageUrl(url));
            document.location.reload();
            setLoading(false);
        }
    }

    function beforeUpload(file) {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'image/jpg';
        if (!isJpgOrPng) {
            showError('You can only upload JPG/PNG/JPEG file!');
        }
        return isJpgOrPng;
    }

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined/> : <PlusOutlined/>}
            <div className="ant-upload-text">Upload</div>
        </div>
    );

    return (
        <>
            {cropImage
                ? <ImgCrop>
                    <Upload
                        name='img'
                        action={url}
                        headers={{authorization: tokenAssembler()}}
                        className="avatar-uploader"
                        listType="picture-card"
                        showUploadList={false}
                        accept={'.jpg,.png,.jpeg'}
                        onChange={onChange}
                        method={method}
                        beforeUpload={beforeUpload}
                    >
                        {imageUrl ? <img src={imageUrl} alt='uploading.img' style={{width: '100%'}}/> : uploadButton}
                    </Upload>
                </ImgCrop>
                : <Upload
                    name='img'
                    action={url}
                    headers={{authorization: tokenAssembler()}}
                    className="avatar-uploader"
                    listType="picture-card"
                    showUploadList={false}
                    accept={'.jpg,.png,.jpeg'}
                    onChange={onChange}
                    method={method}
                    beforeUpload={beforeUpload}
                >
                    {imageUrl ? <img src={imageUrl} alt='uploading.img'/> : uploadButton}
                </Upload>}
        </>
    );
}

export function FileUploader(file, method, url, bodyData) {
    url = process.env.REACT_APP_BASE_URL + url;
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        const formData = new FormData();
        formData.append("img", file, file.name);
        Object.keys(bodyData).map(bdk => {
            formData.append(bdk, bodyData[bdk]);
            return null;
        })
        xhr.open(method, url, true);
        xhr.setRequestHeader('authorization', tokenAssembler());
        xhr.send(formData);
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                return resolve(xhr.response);
            }
        };
    });
}
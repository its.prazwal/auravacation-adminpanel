import React, {useState} from "react";
import SubHeaderBar from "../../Common/SubHeader/SubHeader.component";
import NameStatement from "../../Common/Name_ST/NS.component";

const initialState = {
    chosen: 'INCLUDES'
}

function CostDetailIndex(){
    const [{chosen}, setState] = useState(initialState)
    const subHeads = ['INCLUDES','EXCLUDES']

    function handleChoose(e){
        const {value} = e.target;
        setState(prevState => ({...prevState, chosen: value}))
    }

    return(
        <>
            <SubHeaderBar chosen={chosen} handleChoose={handleChoose} subHeads={subHeads} />
            <NameStatement subHead={chosen} head='COST_DETAILS' NorS='statement' />
        </>
    )
}

export default CostDetailIndex;
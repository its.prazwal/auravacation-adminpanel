import React, {useEffect, useState} from 'react';

import SideBar from "../Sidebar/Sidebar.component";

import LocationIndex from './Location';
import CostDetailIndex from "./CostDetails";
import CompanyProfileIndex from "./CompanyProfile";
import TripFactsIndex from "./TripFacts";
import AdventureActIndex from "./AdventureAct";
import ManageHomepage from "./ManageHomepage";
import ExternalLinksIndex from "./ExternalLinks";

function OptionComponent(props){
    const _optionName = props.match.params.optionName;
    const [Component, setComponent] = useState(<></>);
    const UnknownName = ( <h1>Page Not Found.</h1> )

    useEffect(()=>{
    switch (_optionName) {
        case 'Adventure-Activities':
            setComponent(<AdventureActIndex />);
            break;
        case 'Company-Profile':
            setComponent(<CompanyProfileIndex />);
            break;
        case 'Cost-Details':
            setComponent(<CostDetailIndex />);
            break;
        case 'Manage-Homepage':
            setComponent(<ManageHomepage />);
            break;
        case 'Location':
            setComponent(<LocationIndex />);
            break;
        case 'Trip-Facts':
            setComponent(<TripFactsIndex />);
        case 'External-Links':
            setComponent(<ExternalLinksIndex />);
            break;
        default:
            setComponent(UnknownName);
    }
    },[_optionName]);

    return (
        <>
        <SideBar
            mainNav='Options'
            navs={['Manage Homepage','Location','Cost Details', 'Trip Facts', 'Adventure Activities', 'Company Profile','External Links']}
        />
        <div className="main-content">
            {Component}
        </div>
        </>
    )
}

export default OptionComponent;
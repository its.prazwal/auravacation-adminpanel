import React, {useEffect, useState} from "react";
import httpClient from "../../../utility/httpClient";
import {COMPANY, HOMEPAGE_API, PACKAGE} from "../../../constants/ApiURL.constant";
import {PACKAGE_IMG, DEFAULT} from "../../../constants/IMGURL.constant";

import './ManageHomepage.css';
import {showError, showSuccess} from "../../../utility/Notify";

const initialTaskState = {
    selectingImage: '',
    isLoading: false,
    isSavingLayout: false,
    showLayout: false,
}
const initialDbData = {
    aboutUs: [],
    packages: [],
    whyCompany: [],
    contactDetail: [],
    homepageLayouts: [],
}
const initialHomepageData = {
    layoutName: '',
    displayImage: {
        trekking: '',
        tour: '',
    },
    aboutUsId: '',
    sixPackagesId: [],
    whyCompanyId: '',
    contactDetailId: [],
}


function ManageHomepage() {
    const [imageSet, setImageSet] = useState({
        trekking: [],
        tour: [],
    });
    const [dbData, setDbData] = useState(initialDbData)
    const [homepageData, setHomepageData] = useState(initialHomepageData)
    const [taskState, setTaskState] = useState(initialTaskState);

    useEffect(() => {
        setTaskState(prevState => ({...prevState, isLoading: true}));
        httpClient.get(HOMEPAGE_API, {})
            .then(hm => {
                httpClient.get(PACKAGE.COMMON, {})
                    .then(pack => {
                        const _packages = pack.data;
                        const _trekking = _packages.map(p => {
                            if (p.style === 'Trekking') {
                                return p.image
                            } else {
                                return null;
                            }
                        });
                        const _tour = _packages.map(p => {
                            if (p.style === 'Tour') {
                                return p.image
                            } else {
                                return null;
                            }
                        });
                        setImageSet({
                            trekking: _trekking.filter(img => img !== null),
                            tour: _tour.filter(img => img !== null),
                        })
                        httpClient.get(COMPANY.ABOUT_US, {})
                            .then(about => {
                                httpClient.get(COMPANY.WHY_COMPANY, {})
                                    .then(why => {
                                        httpClient.get(COMPANY.CONTACT_US, {})
                                            .then(cont => {
                                                setDbData(prevState => ({
                                                    ...prevState,
                                                    packages: _packages,
                                                    aboutUs: about.data,
                                                    whyCompany: why.data,
                                                    contactDetail: cont.data,
                                                    homepageLayouts: hm.data,
                                                }))
                                                if (hm.data.length > 0) {
                                                    setHomepageData(hm.data.filter(val => val.selected === true)[0] || initialHomepageData);
                                                }
                                                setTaskState(prevState => ({...prevState, isLoading: false}))
                                            })
                                    })
                            })
                    })
            }).catch(err => console.trace('err: ', err))
    }, [])

    function refreshLayout() {
        httpClient.get(HOMEPAGE_API, {})
            .then(data => setDbData(prevState => ({...prevState, homepageLayouts: data.data})))
            .catch(err => console.trace('err: ', err));
    }

    function selectImage(e) {
        let {name, src} = e.target;
        if (name === 'default') {
            setHomepageData(prevState => ({
                ...prevState,
                displayImage: {
                    ...prevState.displayImage,
                    [taskState.selectingImage]: ''
                }
            }))
        } else {
            src = src.split('/')[6];
            setHomepageData(prevState => ({
                ...prevState,
                displayImage: {...prevState.displayImage, [name]: src.toString()}
            }))
        }
    }

    function closeSelectingImage() {
        setTaskState(prevState => ({...prevState, selectingImage: ''}));
    }

    function selectAboutUs(id) {
        setHomepageData(prevState => ({...prevState, aboutUsId: id}))
    }

    function selectWhyCompany(id) {
        setHomepageData(prevState => ({...prevState, whyCompanyId: id}))
    }

    function selectPackages(id) {
        let arrayUpdater = [...homepageData.sixPackagesId];
        if (homepageData.sixPackagesId.some(pkId => pkId === id)) {
            arrayUpdater = arrayUpdater.filter(pkId => pkId !== id);
        } else {
            if (arrayUpdater.length === 6) {
                showError('limit of 6 has reached.')
                return null;
            } else {
                arrayUpdater.push(id)
            }
        }
        setHomepageData(prevState => ({...prevState, sixPackagesId: arrayUpdater}))
    }

    function selectContact(id) {
        let arrayUpdater = [...homepageData.contactDetailId];
        if (homepageData.contactDetailId.some(cdId => cdId === id)) {
            arrayUpdater = arrayUpdater.filter(cdId => cdId !== id);
        } else {
            if (arrayUpdater.length === 4) {
                showError('limit of 4 has reached.')
                return null;
            } else {
                arrayUpdater.push(id)
            }
        }
        setHomepageData(prevState => ({...prevState, contactDetailId: arrayUpdater}))
    }

    function changeLayoutName(e) {
        const {value} = e.target;
        setHomepageData(prevState => ({...prevState, layoutName: value}));
    }

    function saveLayout(e) {
        e.preventDefault();
        const {name} = e.target;
        if (name === 'addNew') {
            httpClient.post(HOMEPAGE_API, {body: homepageData})
                .then(data => {
                    showSuccess(data.data);
                    refreshLayout();
                })
                .catch(err => console.trace('err: ', err));
        } else {
            httpClient.put(`${HOMEPAGE_API}/${homepageData._id}`, {body: homepageData})
                .then(data => {
                    showSuccess(data.data);
                    refreshLayout();
                })
                .catch(err => console.trace('err: ', err));
        }
        setTaskState(prevState => ({...prevState, isSavingLayout: false}));
    }

    function closeSavingLayout() {
        setTaskState(prevState => ({...prevState, isSavingLayout: false}));
        if (!homepageData._id) setHomepageData(prevState => ({...prevState, layoutName: ''}));
    }

    function selectLayout(id) {
        setHomepageData(dbData.homepageLayouts.filter(hpl => hpl._id === id)[0]);
        setTaskState(prevState => ({...prevState, showLayout: false}))
    }

    function removeLayout(id) {
        let arrayUpdater = [...dbData.homepageLayouts];
        arrayUpdater = arrayUpdater.filter(au => au._id !== id);
        setDbData(prevState => ({...prevState, homepageLayouts: arrayUpdater}));
        httpClient.remove(`${HOMEPAGE_API}/${id}`, {})
            .then(data => showSuccess(data.data))
            .catch(err => console.trace('err: ', err));
    }

    function selectCurrentLayout(){
        httpClient.put(`${HOMEPAGE_API}/${homepageData._id}`, {body: {selected: true}})
            .then(data => {
                showSuccess(`${homepageData.layoutName} is selected for display.`)
                    refreshLayout();
            })
            .catch(err => console.trace('err: ',err));
    }

    function validateData() {
        let _errors = '';
        if (!homepageData.whyCompanyId) _errors = 'Please select why company.';
        if (homepageData.sixPackagesId.length < dbData.packages.length
            && homepageData.sixPackagesId.length < 6) _errors = 'Please select all or at-least six packages.';
        if (homepageData.contactDetailId.length === 0) _errors = 'Please select Contact Detail';
        if (!homepageData.aboutUsId) _errors = 'Please select About us.';
        if (_errors) {
            setTimeout(() => {
                showError(_errors);
            }, 100)
        }
        return !_errors;
    }

    const topButtons = (
        <div className='homepage-top-buttons'>
            <button className='button button-primary'
                    onClick={() => setHomepageData(initialHomepageData)}>
                New Layout
            </button>
            <button className='button button-primary'
                    onClick={() => {
                        if (validateData()) {
                            setTaskState(prevState => ({...prevState, isSavingLayout: true}))
                        }
                    }}>
                Save Layout
            </button>
            <button className='button button-primary'
                    onClick={() => setTaskState(prevState => ({...prevState, showLayout: true}))}>
                Load Layout
            </button>
        </div>
    )
    const selectedHomepageLayout = dbData.homepageLayouts.filter(hl => hl.selected === true);
    let selectedHPLayoutName = 'None'
    if(selectedHomepageLayout.length > 0){
        selectedHPLayoutName = selectedHomepageLayout[0].layoutName;
    }
    return (
        <>
            {taskState.isLoading ?
                <div className='loader-wrapper'>
                    <div className='loader'/>
                </div>
                :
                <div className='homepage-wrapper'>
                    {/*IMAGE SELECTION MODAL*/}
                    {taskState.selectingImage &&
                    <div className='modal-box' id='homepage-selecting-image'>
                        <div className='image-selecting-modal-content'>
                            <span className='modal-close' onClick={closeSelectingImage}>&times;</span>
                            <h2>Select Image for {taskState.selectingImage}...</h2>
                            <div className='ismc-mid-content'>
                                {taskState.selectingImage === 'tour' ?
                                    <img src={DEFAULT.TOUR_IMG} alt='tour.png'
                                         className={`ismc-mid-content-img  
                                 ${homepageData.displayImage[taskState.selectingImage] ? '' : 'ismc-img-selected'}`}
                                         onClick={selectImage}
                                         name='default'
                                    /> :
                                    <img src={DEFAULT.TREK_IMG} alt='trekking.png'
                                         className={`ismc-mid-content-img
                                 ${homepageData.displayImage[taskState.selectingImage] ? '' : 'ismc-img-selected'}`}
                                         onClick={selectImage}
                                         name='default'
                                    />
                                }
                                {imageSet[taskState.selectingImage].map((img, i) => {
                                    return (
                                        <>
                                            <img src={`${PACKAGE_IMG}${img}`} key={i} alt='selecting.png'
                                                 onClick={selectImage}
                                                 name={taskState.selectingImage}
                                                 className={`ismc-mid-content-img 
                                     ${homepageData.displayImage[taskState.selectingImage] === img ? 'ismc-img-selected' : ''}`}
                                            />
                                        </>
                                    )
                                })}
                            </div>
                        </div>
                    </div>}

                    {/*SAVING LAYOUT MODAL*/}
                    {taskState.isSavingLayout &&
                    <div className='modal-box' id='homepage-layout-name'>
                        <div className='homepage-layout-modal-content'>
                            <span className='modal-close'
                                  onClick={closeSavingLayout}>
                            &times;</span>
                            <form>
                                <input
                                    id='layoutName'
                                    name='layoutName'
                                    placeholder='Layout Name'
                                    onChange={changeLayoutName}
                                    value={homepageData.layoutName}
                                    autoFocus={true}
                                />
                                <button
                                    type='submit'
                                    className={`button button-${homepageData.layoutName ? 'primary' : 'disabled'}`}
                                    name='addNew'
                                    onClick={saveLayout}
                                    {...Object.assign({}, homepageData.layoutName ? null : {disabled: true})}
                                >Create New
                                </button>
                                <button
                                    type='submit'
                                    className={`button button-${(homepageData.layoutName && homepageData._id) ? 'secondary' : 'disabled'}`}
                                    {...Object.assign({}, (homepageData.layoutName && homepageData._id) ? null : {disabled: true})}
                                    onClick={saveLayout}
                                    name='saveOld'
                                >Save
                                </button>
                            </form>
                        </div>
                    </div>}

                    {/*SHOW LAYOUTS MODAL*/}
                    {taskState.showLayout &&
                    <div className='modal-box' id='homepage-show-layouts'>
                        <div className='homepage-show-layouts-content'>
                            <span
                                className='modal-close'
                                onClick={() => setTaskState(prevState => ({...prevState, showLayout: false}))}>
                                &times;</span>
                            {dbData.homepageLayouts.length > 0 ?
                                dbData.homepageLayouts.map(hl => {
                                    return (
                                        <div className={`hslc-each ${hl._id === homepageData._id ? 'hslc-each-selected' : ''}`}>
                                            <h3 key={hl._id}
                                                onClick={() => selectLayout(hl._id)}
                                            >{hl.layoutName} - <i>{new Date().toDateString()}</i>
                                            </h3>
                                            <span onClick={() => removeLayout(hl._id)}>
                                        <i className="far fa-trash-alt"/></span>
                                        </div>
                                    )
                                }) : <div className='No-Data'/>}
                        </div>
                    </div>}

                    {topButtons}
                    <h1>- - - Manage Your Homepage - - - <br/>
                        <i>Current Display Layout: <b>{homepageData.layoutName || 'None'}</b></i>
                        {homepageData.layoutName &&
                        <button
                            className={`button button-${selectedHPLayoutName === homepageData.layoutName ? 'disabled' : 'secondary'}`}
                            {...Object.assign({},
                                selectedHPLayoutName === homepageData.layoutName ? {disabled:true} : null)}
                            onClick={selectCurrentLayout}
                        >
                            Select This Layout</button>
                        }<br/>
                        <i>Selected Homepage Layout: <b>{selectedHPLayoutName}</b></i>
                    </h1>
                    <div className='homepage-two-image-wrapper'>
                        <h2>Landing Images</h2>
                        <p>can leave to default or choose new images.</p>
                        <div className='htiw-trekking'>
                            <span className='htiw-click-image' id='htiw-click-image-trekking'
                                    onClick={() => {
                                    setTaskState(prevState => ({...prevState, selectingImage: 'trekking'}))
                                    }}
                            > Click to change...</span>
                            {homepageData.displayImage.trekking ?
                                <img src={`${PACKAGE_IMG}${homepageData.displayImage.trekking}`}
                                     alt='trekking.png'/>
                                : <img src={DEFAULT.TREK_IMG} alt='trekking.png'/>}
                            <h3>Trekking In Nepal</h3>
                        </div>
                        <div className='htiw-tour'>
                <span className='htiw-click-image' id='htiw-click-image-tour'
                      onClick={() => {
                          setTaskState(prevState => ({...prevState, selectingImage: 'tour'}))
                      }}
                > Click to change...</span>
                            {homepageData.displayImage.tour ?
                                <img src={`${PACKAGE_IMG}${homepageData.displayImage.tour}`} alt='tour.png'/>
                                : <img src={DEFAULT.TOUR_IMG} alt='tour.png'/>}
                            <h3>Tour In Nepal</h3>
                        </div>
                    </div>
                    <div className='homepage-aboutUs-wrapper'>
                        <h2>About Us</h2>
                        <p>choose 1 detail</p>
                        {dbData.aboutUs.length > 0 ? dbData.aboutUs.map(about => {
                                return (
                                    <div className={`homepage-aboutUs-each 
                                        ${homepageData.aboutUsId === about._id ? 'haue-selected' : ''}`}
                                         key={about._id}
                                         onClick={() => selectAboutUs(about._id)}
                                    >
                                        <h2>{about.title}</h2>
                                        <p>{about.description}</p>
                                    </div>
                                )
                            }) :
                            <div className='No-Data'/>}
                    </div>
                    <div className='homepage-packages-wrapper'>
                        <h2>Packages</h2>
                        <p>select 6 packages that will be displayed at front</p>
                        {dbData.packages.length > 0 ? dbData.packages.map(pack => {
                                return (
                                    <div className={`homepage-packages-each
                                ${homepageData.sixPackagesId.some(pkId => pkId === pack._id) ? 'hpke-selected' : ''}
                                `}
                                         key={pack._id}
                                         onClick={() => selectPackages(pack._id)}
                                    >
                                        <img src={`${PACKAGE_IMG}/${pack.image}`} alt='package.png'/>
                                        <h2>{pack.packageName} - {pack.country} - {pack.region || pack.adventureType}</h2>
                                        <p>
                                            {pack.tripFacts.duration} Days
                                            - USD {pack.tripFacts.price}
                                        </p>
                                    </div>
                                )
                            }) :
                            <div className='No-Data'/>}
                    </div>
                    <div className='homepage-whyCompany-wrapper'>
                        <h2>Why Company</h2>
                        <p>choose 1 detail</p>
                        {dbData.whyCompany.length > 0 ? dbData.whyCompany.map(why => {
                                return (
                                    <div className={`homepage-whyCompany-each 
                                        ${homepageData.whyCompanyId === why._id ? 'hwce-selected' : ''}`}
                                         key={why._id}
                                         onClick={() => selectWhyCompany(why._id)}
                                    >
                                        <h2>{why.title}</h2>
                                        <p>{why.description}</p>
                                    </div>
                                )
                            }) :
                            <div className='No-Data'/>}
                    </div>
                    <div className='homepage-contactDetail-wrapper'>
                        <h2>contact detail</h2>
                        <p>select at least 1 and not more than 4</p>
                        {dbData.contactDetail.length > 0 ? dbData.contactDetail.map(cont => {
                                return (
                                    <div className={`homepage-contactDetail-each
                                ${homepageData.contactDetailId.some(pkId => pkId === cont._id) ? 'hcde-selected' : ''}
                                `}
                                         key={cont._id}
                                         onClick={() => selectContact(cont._id)}
                                    >
                                        <h2><b>{cont.officeName}</b> - {cont.contactPerson} - {cont.phone}</h2>
                                    </div>
                                )
                            }) :
                            <div className='No-Data'/>}
                    </div>
                </div>}
        </>
    )
}

export default ManageHomepage;
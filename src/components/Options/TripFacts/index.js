import React, {useState} from "react";
import SubHeaderBar from "../../Common/SubHeader/SubHeader.component";
import NameStatement from "../../Common/Name_ST/NS.component";
import TDNDComponent from "../../Common/TD_ND/TD_ND.component";

const initialState = {
    chosen: 'STYLES'
}

function TripFactsIndex() {
    const [{chosen}, setState] = useState(initialState)
    const subHeads = ['STYLES', 'HIGHLIGHTS', 'ACCOMMODATION', 'TRANSPORTATION']

    function handleChoose(e) {
        const {value} = e.target;
        setState(prevState => ({...prevState, chosen: value}))
    }

    return (
        <>
            <SubHeaderBar chosen={chosen} handleChoose={handleChoose} subHeads={subHeads}/>
            {chosen === 'STYLES'
                ? <TDNDComponent head='TRIP_FACTS' subHead={chosen} TDorND='ND'/>
                : <NameStatement NorS={chosen === 'HIGHLIGHTS' ? 'statement' : 'name'} head='TRIP_FACTS'
                                 subHead={chosen}/>
            }
        </>
    )
}

export default TripFactsIndex;
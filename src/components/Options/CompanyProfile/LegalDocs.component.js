import React, {useEffect, useState} from "react";
import httpClient from "../../../utility/httpClient";
import {COMPANY} from "../../../constants/ApiURL.constant";
import {Form, Input} from "antd";
import {FileUploader} from "../../Common/Uploader";

const initialFormState = {docImage: '', title: ''}

function LegalDocs() {
    const [formState, setFormState] = useState(initialFormState)
    const [errors, setErrors] = useState({});
    const [dbData, setDbData] = useState([]);
    const [uploading, setUploading] = useState(false);
    const [viewImage, setViewImage] = useState('');

    useEffect(() => {
        refreshData();
    }, [])

    function refreshData() {
        httpClient.get(COMPANY.LEGAL_DOCS, {})
            .then(data => setDbData(data.data))
            .catch(err => console.trace('err: ', err));
    }

    function handleChange(e) {
        let {type, name, value, files} = e.target;
        if (type === 'file') {
            if (files.length) {
                if (files[0].type === 'image/jpeg' || files[0].type === 'image/jpg' || files[0].type === 'image/png') {
                    value = files[0];
                } else {
                    setErrors({docImage: 'Invalid file format.'})
                    return null;
                }
            } else {
                return null;
            }
        }
        setFormState(prevState => ({...prevState, [name]: value}));
        delete errors[name];
    }

    function handleSubmit(e) {
        e.preventDefault();
        if (validate()) {
            setUploading(true);
            FileUploader(formState.docImage, 'POST', COMPANY.LEGAL_DOCS, {title: formState.title})
                .then(data => {
                    refreshData();
                    setFormState(initialFormState);
                    setUploading(false);
                })
                .catch(err => console.trace('err: ', err))
        }
    }

    function handleRemove(e) {
        e.preventDefault();
        const {value} = e.target;
        httpClient.remove(`${COMPANY.LEGAL_DOCS}/${value}`, {})
            .then(data => refreshData())
            .catch(err => console.trace('err: ', err))
    }

    function removeImage() {
        setFormState(prevState => ({...prevState, docImage: ''}))
    }

    function viewLegalDoc(ld) {
        setViewImage(ld);
        document.getElementById('legalDocs-view').style.display = 'block';
    }

    function hideLegalDoc() {
        setViewImage('');
        document.getElementById('legalDocs-view').style.display = 'none';
    }

    if (viewImage.docImage) {
        document.addEventListener('keydown', (evt) => evt.key === 'Escape' && hideLegalDoc(), false);
    }

    function validate() {
        let _errors = {};
        if (!formState.title) _errors.title = 'Title is required.'
        if (!formState.docImage) _errors.docImage = 'Please select an image file.'
        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }

    return (
        <>
            <div className='modal-box' id='legalDocs-view'>
                <div className='legalDocs-content'>
                    <span className='modal-close' onClick={hideLegalDoc}>&times;</span>
                    <h1>{viewImage.title}</h1>
                    <img src={`${process.env.REACT_APP_IMAGE_URL}/legalDocsIMG/${viewImage.docImage}`} alt='noImage'/>
                </div>
            </div>
            <Form onSubmitCapture={handleSubmit} className='legalDocs-form'>
                <Form.Item
                    validateStatus={errors.title ? 'error' : formState.title && 'success'}
                    hasFeedback
                    help={formState.title ? null : errors.title || null}
                >
                    <Input
                        type='text'
                        placeholder='Image title to display...'
                        name='title'
                        onChange={handleChange}
                        autoFocus={true}
                        value={formState.title}
                    />
                </Form.Item>
                <div className='legalDocs-image'>
                    <Input id='legalDocs-fileInput' type='file' name='docImage' onChange={handleChange}
                           accept=".jpeg, .jpg, .png"/>
                    <button
                        type='button'
                        className='button button-secondary'
                        onClick={() => document.getElementById('legalDocs-fileInput').click()}>
                        Select Image
                    </button>
                    {formState.docImage &&
                    <p>{formState.docImage.name} <span onClick={removeImage}>&times;</span></p>
                    }
                    {errors.docImage && <><br/> <span className='error-message'>{errors.docImage}</span></>}
                </div>
                <button type='submit' className={`button button-${uploading ? 'disabled' : 'primary'}`}
                        {...Object.assign({}, uploading ? {disabled: true} : null)}
                >
                    {uploading ? 'Uploading' : 'Upload'}
                </button>
            </Form>
            <div className='legalDocs-data'>
                {dbData.length ?
                    dbData.map((ld, i) => {
                        return (
                            <div className='legalDocs-eachData' key={i}>
                                <button value={ld._id}
                                        onClick={handleRemove}
                                        name='removeSt'
                                        className='button button-closeIcon'>
                                </button>
                                <p>{ld.title}</p>
                                <img src={`${process.env.REACT_APP_IMAGE_URL}/legalDocsIMG/${ld.docImage}`}
                                     alt='noImage'
                                     onClick={() => viewLegalDoc(ld)}
                                />
                            </div>
                        )
                    }) : <div className='No-Data'/>
                }
            </div>
        </>
    )
}

export default LegalDocs;
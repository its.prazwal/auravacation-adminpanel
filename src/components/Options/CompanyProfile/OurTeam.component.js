import React, {useEffect, useRef, useState} from 'react';
import {Form, Input} from 'antd';
import * as APIURL from '../../../constants/ApiURL.constant';
import {OUR_TEAM_IMG} from '../../../constants/IMGURL.constant';
import httpClient from "../../../utility/httpClient";
import {ImageUploader} from "../../Common/Uploader";

const {TextArea} = Input;
const initialTaskState = {
    isAddNew: true,
    isEditing: '',
    viewData: {},
}
const initialFormState = {
    new: {
        fullName: '',
        designation: '',
        message: '',
        image: ''
    },
    old: {}
}

function OurTeam() {
    const [taskState, setTaskState] = useState(initialTaskState);
    const [formState, setFormState] = useState(initialFormState);
    const [dbData, setDbData] = useState([]);
    const newInputRef = useRef(null);

    useEffect(() => {
        refreshData();
        document.getElementById('company-ourTeam-form').style.height = '0px';

    }, [])

    function refreshData() {
        httpClient.get(APIURL.COMPANY.OUR_TEAM, {})
            .then(data => setDbData(data.data))
            .catch(err => console.trace('err: ', err));
    }

    function showOrHideForm() {
        document.getElementById('company-ourTeam-form').style.height = taskState.isAddNew ? '350px' : '0px';
        setTaskState(prevState => ({...prevState, isAddNew: !taskState.isAddNew}));
        setFormState(prevState => ({...prevState, new: initialFormState.new}));
        newInputRef.current.focus();
    }

    function handleChange(e) {
        const {id, name, value} = e.target;
        if (id === 'newFN' || id === 'newD' || id === 'newM') {
            setFormState(prevState => ({...prevState, new: {...prevState.new, [name]: value}}))
        } else {
            setFormState(prevState => ({...prevState, old: {...prevState.old, [name]: value}}))
        }
    }

    function handleAddNew(e) {
        httpClient.post(APIURL.COMPANY.OUR_TEAM, {body: formState.new})
            .then(data => {
                showOrHideForm();
                refreshData();
            })
            .catch(err => {
                console.trace('err: ', err);
            })
        e.preventDefault();
    }

    function hideView() {
        setTaskState(prevState => ({...prevState, viewData: {}}))
        document.getElementById("ED-view-wrapper").style.display = "none";
    }

    function handleEachData(e) {
        const {name, value} = e.target;
        const editingData = dbData.filter(dd => dd._id === value)[0];
        switch (name) {
            case 'edit':
                setTaskState(prevState => ({...prevState, isEditing: value}))
                setFormState(prevState => ({...prevState, old: editingData}))
                break;
            case 'cancel':
                setTaskState(prevState => ({...prevState, isEditing: ''}))
                setFormState(prevState => ({...prevState, old: {}}))
                break;
            case 'save':
                httpClient.put(APIURL.COMPANY.OUR_TEAM, {body: formState.old})
                    .then(data => {
                        refreshData();
                        setTaskState(prevState => ({...prevState, isEditing: ''}))
                        setFormState(prevState => ({...prevState, old: {}}))
                    })
                    .catch(err => console.trace('err: ', err))
                break;
            case 'delete':
                httpClient.remove(APIURL.COMPANY.OUR_TEAM, {body: {_id: value}})
                    .then(data => refreshData())
                    .catch(err => console.trace('err: ', err))
                break;
            case 'view':
                setTaskState(prevState => ({...prevState, viewData: dbData.filter(dbd => dbd._id === value)[0]}))
                document.getElementById("ED-view-wrapper").style.display = "block";
                break;
            default:
                break;
        }
    }

    const addNewButton = <button
        className={`button button-${taskState.isAddNew ? 'primary' : 'cancel'}`}
        onClick={showOrHideForm}
    >
        {!taskState.isAddNew ? 'Cancel' : 'Add New'}
    </button>
    const eachDataButtons = (ed) => {
        return (
            <>
                <button
                    name={taskState.isEditing === ed._id ? 'cancel' : 'delete'}
                    className={`button button-${taskState.isEditing === ed._id ? 'cancel' : 'remove'}`}
                    value={ed._id}
                    onClick={handleEachData}
                >
                    {taskState.isEditing === ed._id ? 'Cancel' : 'Delete'}
                </button>
                <button
                    name={taskState.isEditing === ed._id ? 'save' : 'edit'}
                    className={`button button-${taskState.isEditing === ed._id ? 'primary' : 'secondary'}`}
                    value={ed._id}
                    onClick={handleEachData}
                >
                    {taskState.isEditing === ed._id ? 'Save' : 'Edit'}
                </button>
                {taskState.isEditing !== ed._id &&
                <button
                    className={`button button-primary`}
                    value={ed._id}
                    onClick={handleEachData}
                    name='view'
                >
                    View
                </button>
                }
            </>
        )
    }
    const viewDataDiv = <div className="modal-box" id="ED-view-wrapper">
        <div className="OT-view-data" id='ED-view-data'>
          <span className="modal-close" onClick={hideView}>
            &times;
          </span>
            <div className='OT-viewData-head'>
                <img src={taskState.viewData.image
                    ? `${OUR_TEAM_IMG}${taskState.viewData.image}`
                    : require('../../../profile-pic.png')
                } alt='member.png'/>
                <h2>{taskState.viewData.fullName}<br/><b>{taskState.viewData.designation}</b></h2>
            </div>
            <p>{taskState.viewData.message}</p>
        </div>
    </div>

    return (
        <>
            {viewDataDiv}
            <div className='company-ourTeam-wrapper'>
                {addNewButton}
                <Form id='company-ourTeam-form'>
                    <Input
                        id='newFN'
                        ref={newInputRef}
                        allowClear
                        name='fullName'
                        placeholder='full Name of TEAM MEMBER...'
                        value={formState.new.fullName}
                        onChange={handleChange}
                    />
                    <Input
                        id='newD'
                        allowClear
                        name='designation'
                        placeholder='designation of TEAM MEMBER...'
                        value={formState.new.designation}
                        onChange={handleChange}
                    />
                    <TextArea
                        id='newM'
                        allowClear
                        name='message'
                        placeholder='message from TEAM MEMBER...'
                        onChange={handleChange}
                        value={formState.new.message}
                    />
                    {(formState.new.fullName && formState.new.designation) &&
                    <button className="button button-primary" onClick={handleAddNew}>
                        Add
                    </button>
                    }
                </Form>
                <div className='ourTeam-data-wrapper'>
                    {dbData.length > 0 ?
                        dbData.map((ot, i) => {
                        const imageUrl = `${OUR_TEAM_IMG}${ot.image}`
                        return (
                            <div className='ourTeam-eachData' key={i}>
                                <div className='ourTeam-eachData-detail-top'>
                                    {eachDataButtons(ot)}
                                    {taskState.isEditing === ot._id ?
                                        <>
                                            <Input
                                                autoFocus={true}
                                                name='fullName'
                                                placeholder={ot.fullName}
                                                value={formState.old.fullName}
                                                onChange={handleChange}
                                            />
                                            <Input
                                                name='designation'
                                                placeholder={ot.designation}
                                                value={formState.old.designation}
                                                onChange={handleChange}
                                            />
                                        </>
                                        : <h2>{ot.fullName} | <b>{ot.designation}</b></h2>
                                    }
                                </div>
                                <div className='ourTeam-eachData-image'>
                                    <ImageUploader
                                        url={`${APIURL.COMPANY.OUR_TEAM}/upload/${ot._id}`}
                                        method={'PUT'}
                                        saveImgUrl={ot.image ? imageUrl : ''}
                                        cropImage={true}
                                    />
                                </div>
                                <div className='ourTeam-eachData-detail'>
                                    {taskState.isEditing === ot._id ?
                                        <TextArea
                                            name='message'
                                            placeholder={`message from ${ot.fullName}...`}
                                            value={formState.old.message}
                                            onChange={handleChange}
                                        />
                                        : <p>{ot.message}</p>
                                    }
                                </div>
                            </div>

                        )
                    }):<div className='No-Data'/>}
                </div>
            </div>
        </>
    )
}

export default OurTeam;
import React, {useState} from "react";
import SubHeaderBar from "../../Common/SubHeader/SubHeader.component";
import TDNDComponent from "../../Common/TD_ND/TD_ND.component";
import OurTeam from "./OurTeam.component";
import LegalDocs from "./LegalDocs.component";
import './CompanyProfile.css';
import ContactUsComponent from "./ContactUs.component";

const initialState = {
    chosen: 'ABOUT US'
}

function CompanyProfileIndex(){
    const [{chosen}, setState] = useState(initialState)
    const subHeads = ['ABOUT US','OUR TEAM', 'PRIVACY POLICY','WHY COMPANY','CONTACT US', 'LEGAL DOCUMENTS']

    function handleChoose(e){
        const {value} = e.target;
        setState(prevState => ({...prevState, chosen: value}))
    }

    return(
        <>
            <SubHeaderBar chosen={chosen} handleChoose={handleChoose} subHeads={subHeads} />
            {chosen === 'ABOUT US' || chosen === 'PRIVACY POLICY' || chosen === 'WHY COMPANY'
            ? <TDNDComponent subHead={chosen} head='COMPANY' TDorND='TD' /> : null}
            {chosen === 'OUR TEAM' && <OurTeam />}
            {chosen === 'LEGAL DOCUMENTS' && <LegalDocs />}
            {chosen === 'CONTACT US' && <ContactUsComponent/>}
        </>
    )
}

export default CompanyProfileIndex;
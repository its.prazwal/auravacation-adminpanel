import React, {useEffect, useRef, useState} from "react";
import httpClient from "../../../utility/httpClient";
import {COMPANY} from "../../../constants/ApiURL.constant";
import {Form, Input} from 'antd';
import {showSuccess} from "../../../utility/Notify";

const initialTaskState = {
    isAddNew: false,
    isEditing: false,
    showForm: false,
}

const initialFormState = {
    officeName: '',
    contactPerson: '',
    emailId: '',
    phone: '',
    address: '',
}


function ContactUsComponent() {
    const [dbData, setDbData] = useState([]);
    const [formState, setFormState] = useState(initialFormState);
    const [taskState, setTaskState] = useState(initialTaskState);
    const [errors, setErrors] = useState({});
    const newInputRef = useRef();

    useEffect(() => {
        refreshData();
    }, [])

    function refreshData() {
        httpClient.get(COMPANY.CONTACT_US, {})
            .then(data => {
                setDbData(data.data)
            })
            .catch(err => console.trace('err: ', err));
    }

    useEffect(() => {
        document.getElementById('contactUs-form').style.height =
            taskState.showForm ? '200px' : '0px';
        if (!taskState.showForm) {
            setTaskState(initialTaskState);
            setFormState(initialFormState);
            setErrors({});
        }
        newInputRef.current.focus();
    }, [taskState.showForm])


    function handleChange(e) {
        const { name, value} = e.target;
        setFormState(prevState => ({
            ...prevState, [name]: value
        }))
        delete errors[name];
    }

    function handleSubmit(e) {
        e.preventDefault();
        const {name} = e.target;
        if (validateData()) {
            if (name === 'addNew') {
                httpClient.post(COMPANY.CONTACT_US, {body: formState})
                    .then(data => {
                        refreshData();
                        setFormState(initialFormState);
                        setTaskState(initialTaskState);
                    })
            } else {
                httpClient.put(COMPANY.CONTACT_US, {body: formState})
                    .then(data => {
                        refreshData();
                        setFormState(initialFormState);
                        setTaskState(initialTaskState);
                    })
            }
        }
    }

    function validateData() {
        let _errors = {};
        if (!formState.officeName) _errors.officeName = 'Office Name is required.';
        if (!formState.contactPerson) _errors.contactPerson = 'Contact Person name is required.';
        if (!formState.emailId) _errors.emailId = 'Email Address is required.';
        if (!formState.phone) _errors.phone = 'Contact number is required.';
        if (!formState.address) _errors.address = 'Address is required.';
        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }

    function handleEdit(e) {
        setTaskState(prevState => ({...prevState, isEditing: true, showForm: true}))
        const {value} = e.target;
        setFormState(dbData.filter(e => e._id === value)[0]);
    }

    function handleDelete(e) {
        const {value} = e.target;
        httpClient.remove(COMPANY.CONTACT_US, {body: {_id: value}})
            .then(data => {
                showSuccess(data.data);
                refreshData();
            })
            .catch(err => console.trace('err: ',err));
    }

    const addNewButton = <button
        className={`button button-${!taskState.showForm ? 'primary' : 'cancel'}`}
        onClick={() => setTaskState(prevState => ({
            ...prevState, showForm: !taskState.showForm,
            isAddNew: !taskState.isAddNew
        }))}
    >
        {taskState.showForm ? 'cancel' : 'Add New'}
    </button>

    return (
        <div className='contactUs-wrapper'>
            {addNewButton}
            <Form id='contactUs-form' className='contactUs-form' onSubmitCapture={handleSubmit}
                  name={taskState.isEditing ? 'saveEdited' : 'addNew'}
            >
                <Form.Item
                    validateStatus={errors.officeName && 'error'}
                    hasFeedback
                    help={errors.officeName || null}
                    style={{width: '25%'}}
                >
                    <Input
                        id='new'
                        allowClear
                        name='officeName'
                        placeholder='Office Name'
                        value={formState.officeName}
                        onChange={handleChange}
                        ref={newInputRef}
                    />
                </Form.Item>
                <Form.Item
                    validateStatus={errors.contactPerson && 'error'}
                    hasFeedback
                    help={errors.contactPerson || null}
                    style={{width: '25%'}}
                >
                    <Input
                        id='new'
                        allowClear
                        name='contactPerson'
                        placeholder='Contact person'
                        value={formState.contactPerson}
                        onChange={handleChange}
                    />
                </Form.Item>
                <Form.Item
                    validateStatus={errors.emailId && 'error'}
                    hasFeedback
                    help={errors.emailId || null}
                    style={{width: '45%'}}
                >
                    <Input
                        id='new'
                        allowClear
                        name='emailId'
                        placeholder='Email Address'
                        value={formState.emailId}
                        onChange={handleChange}
                    />
                </Form.Item>
                <Form.Item
                    validateStatus={errors.phone && 'error'}
                    hasFeedback
                    help={errors.phone || null}
                    style={{width: '25%'}}
                >
                    <Input
                        id='new'
                        allowClear
                        name='phone'
                        placeholder='Contact Number'
                        value={formState.phone}
                        onChange={handleChange}
                        type='number'
                    />
                </Form.Item>
                <Form.Item
                    validateStatus={errors.address && 'error'}
                    hasFeedback
                    help={errors.address || null}
                    style={{width: '71%'}}
                >
                    <Input
                        id='new'
                        allowClear
                        name='address'
                        placeholder='Address'
                        value={formState.address}
                        onChange={handleChange}
                    />
                </Form.Item>
                <button
                    type='submit'
                    name={taskState.isEditing ? 'saveEdited' : 'addNew'}
                    className={`button button-${Object.keys(errors).length === 0 ? 'primary' : 'disabled'}`}
                    {...Object.assign({}, Object.keys(errors).length === 0 ? null : {disabled: true})}
                    onClick={handleSubmit}
                >
                    {taskState.isEditing ? 'Save' : 'Add'}
                </button>
            </Form>
            < div
                className='contactDetail-wrapper'
            >
                {dbData.length > 0 ?
                    dbData.map((detail, i) => {
                        return (
                            <div className="contactDetail-data" key={i}>
                                <h2>{detail.officeName}</h2>
                                <h4>{detail.contactPerson}</h4>
                                <p>
                                    <b>Email:</b> {detail.emailId}
                                    <br/>
                                    <b>Phone:</b> {detail.phone}
                                    <br/>
                                    <b>Address:</b> {detail.address}
                                </p>
                                <button
                                    type='button'
                                    className={`button button-${taskState.isAddNew ? 'disabled' : 'secondary'}`}
                                    value={detail._id}
                                    onClick={handleEdit}
                                    {...Object.assign({}, taskState.isAddNew ? {disabled: true} : null)}
                                >Edit
                                </button>
                                <button
                                    type='button'
                                    className={`button button-${taskState.isEditing ? 'disabled-2' : 'remove'}`}
                                    value={detail._id}
                                    onClick={handleDelete}
                                    {...Object.assign({}, taskState.isEditing ? {disabled: true}:null)}
                                >Delete
                                </button>
                            </div>
                        );
                    })
                    : <div className='No-Data'/>
                }
            </div>
        </div>
    )
}

export default ContactUsComponent;
import React, {useState} from "react";
import SubHeaderBar from "../../Common/SubHeader/SubHeader.component";
import TDNDComponent from "../../Common/TD_ND/TD_ND.component";

const initialState = {
    chosen: 'COUNTRY'
}

function LocationIndex() {
    const [{chosen}, setState] = useState(initialState)
    const subHeads = ['COUNTRY', 'REGION', 'PLACES']

    function handleChoose(e) {
        const {value} = e.target;
        setState(prevState => ({...prevState, chosen: value}))
    }
    return (
        <>
            <SubHeaderBar chosen={chosen} handleChoose={handleChoose} subHeads={subHeads}/>
            <TDNDComponent head='LOCATION' subHead={chosen} TDorND='ND'
                           additions={chosen === 'REGION' ? ['MA', 'OI'] : chosen === 'PLACES' ? ['MA'] : null}/>
        </>
    )
}

export default LocationIndex;
import React, {useEffect, useState} from "react";
import {Form} from "antd";
import {associated, followOn, recommended, weAccept} from './DefaultExternalLinks';

function ExternalLinksIndex() {
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(()=>{
        setIsLoaded(true);
    })

    return (
        <>
            {isLoaded ?
                <div className='external-links-wrapper'>
                    <div className='external-link-each'>
                        <h2>{associated.displayName}</h2>
                        {Object.keys(associated.data).map((ad, i)=>{
                            return(
                                <>
                                <h3>{associated.data[ad].name}</h3>
                                <input value={associated.data[ad].extLink}/>
                                </>
                            )
                        })}
                        <div></div>
                    </div>
                </div>
                : <div className='loader-wrapper'>
                    <div className='loader'/>
                </div>}
        </>
    )
}

export default ExternalLinksIndex;
import React, {useState} from "react";
import httpClient from "../../../utility/httpClient";

function ExtLinksForm({label, savedValue = '', setEditing, refreshData}) {
    const [formText, setFormText] = useState(savedValue);
    const [info, setInfo] = useState('');

    function handleChange(e) {
        const {name, value} = e.target;
        setFormText(value);
        delete info[name];
    }

    function validation() {
        if (!formText) setInfo('There will be no link to redirect.');
    }

    function handleSubmit(e) {
        e.preventDefault();
        if (validation()) {
            httpClient.put({body: {[label]: formText}})
                .then(data => {
                    refreshData();
                })
                .catch(err => console.trace('err: ', err));
            setEditing(false);
        }
    }

    const submitButton = <button
        onClick={handleSubmit}
        className={`button button-${Object.keys(info).length === 0 ? 'secondary' : 'primary'}`}
        {...Object.assign({}, Object.keys(info).length === 0 ? null : {disabled: true})}>
        Save
    </button>

    return (
        <Form onSubmitCapture={handleSubmit}>
            <Form.Item
                validateStatus={
                    onTyping ? 'validating' :
                        info[label] ? 'error' : formText && 'success'
                }
                hasFeedback
                help={onTyping ? null : info[label] || null}
            >
                <Input
                    id={label}
                    name={label}
                    onChange={handleChange}
                    placeholder={label}
                    value={formText}
                    suffix={submitButton}
                    autoFocus={true}
                    onInput={() => {
                        setOnTyping(true);
                        setTimeout(function () {
                            setOnTyping(false)
                        }, 2000);
                    }}
                />
            </Form.Item>
        </Form>
    )
}

export default ExtLinksForm;
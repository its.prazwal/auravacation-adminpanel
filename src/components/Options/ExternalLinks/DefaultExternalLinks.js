export const associated = {
    displayName: 'Associated with',
    data: {
        NTB: {
            name: 'Nepal Tourism Board',
            link: '',
        },
        TAAN: {
            name: 'Trekking Agencies Association of Nepal',
            link: '',
        },
        NMA: {
            name: 'Nepal Mountaineering Association',
            link: '',
        },
        KEEP: {
            name: 'Kathmandu Environmental Education Project',
            link: '',
        },
        MoFA: {
            name: 'Ministry of Foreign Affairs',
            link: '',
        },
    }
}

export const weAccept = {
    displayName: 'We Accept',
    data:{
        VISA:{
            name: "Visa Card",
            link: '',
        },
        MASTER:{
            name: "Master Card",
            link: '',
        },
        AMEXP:{
            name: "American Express",
            link: '',
        },
        DISCOVERY:{
            name: "Discovery Card",
            link: '',
        },

    }
}

export const followOn = {
    displayName: 'Social Networks',
    data: {
        FB:{
            name: 'Facebook',
            link: '',
        },
        INSTA:{
            name: 'Instagram',
            link: '',
        },
        TWT:{
            name: 'Twitter',
            link: '',
        },
        SKP:{
            name: 'Skype',
            link: '',
        },
        LI:{
            name: 'LinkedIn',
            link: '',
        }
    }
}
export const recommended = {
    displayName: 'Recommended On',
    data: {
        TA:{
            name: 'Trip Advisor',
            link: '',
        }
    }
}

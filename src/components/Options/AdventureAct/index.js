import React, {useEffect, useRef, useState} from 'react';
import {Form, Input} from 'antd';
import {ADVENTURE_ACT} from '../../../constants/ApiURL.constant';
import {ADVENTURE_ACT_IMG} from '../../../constants/IMGURL.constant';
import httpClient from "../../../utility/httpClient";
import {ImageUploader} from "../../Common/Uploader";
import './adventureAct.css';

const {TextArea} = Input;
const initialTaskState = {
    isAddNew: true,
    isEditing: '',
    viewData: {},
}
const initialFormState = {
    new: {
        name: '',
        detail: '',
        image: ''
    },
    old: {}
}

function AdventureActivities() {
    const [taskState, setTaskState] = useState(initialTaskState);
    const [formState, setFormState] = useState(initialFormState);
    const [dbData, setDbData] = useState([]);
    const newInputRef = useRef(null);

    useEffect(() => {
        refreshData();
        document.getElementById('company-ourTeam-form').style.height = '0px';

    }, [])

    function refreshData() {
        httpClient.get(ADVENTURE_ACT, {})
            .then(data => {
                setDbData(data.data);
            })
            .catch(err => console.trace('err: ', err));
    }

    function showOrHideForm() {
        document.getElementById('company-ourTeam-form').style.height = taskState.isAddNew ? '350px' : '0px';
        setTaskState(prevState => ({...prevState, isAddNew: !taskState.isAddNew}));
        setFormState(prevState => ({...prevState, new: initialFormState.new}));
        newInputRef.current.focus();
    }

    function handleChange(e) {
        const {id, name, value} = e.target;
        if (id === 'newD' || id === 'newN') {
            setFormState(prevState => ({...prevState, new: {...prevState.new, [name]: value}}))
        } else {
            setFormState(prevState => ({...prevState, old: {...prevState.old, [name]: value}}))
        }
    }

    function handleAddNew(e) {
        httpClient.post(ADVENTURE_ACT, {body: formState.new})
            .then(data => {
                showOrHideForm();
                refreshData();
            })
            .catch(err => {
                console.trace('err: ', err);
            })
        e.preventDefault();
    }

    function hideView() {
        setTaskState(prevState => ({...prevState, viewData: {}}))
        document.getElementById("ED-view-wrapper").style.display = "none";
    }

    function handleEachData(e) {
        const {name, value} = e.target;
        const editingData = dbData.filter(dd => dd._id === value)[0];
        switch (name) {
            case 'edit':
                setTaskState(prevState => ({...prevState, isEditing: value}))
                setFormState(prevState => ({...prevState, old: editingData}))
                break;
            case 'cancel':
                setTaskState(prevState => ({...prevState, isEditing: ''}))
                setFormState(prevState => ({...prevState, old: {}}))
                break;
            case 'save':
                httpClient.put(ADVENTURE_ACT, {body: formState.old})
                    .then(data => {
                        refreshData();
                        setTaskState(prevState => ({...prevState, isEditing: ''}))
                        setFormState(prevState => ({...prevState, old: {}}))
                    })
                    .catch(err => console.trace('err: ', err))
                break;
            case 'delete':
                httpClient.remove(ADVENTURE_ACT, {body: {_id: value}})
                    .then(data => refreshData())
                    .catch(err => console.trace('err: ', err))
                break;
            case 'view':
                setTaskState(prevState => ({...prevState, viewData: dbData.filter(dbd => dbd._id === value)[0]}))
                document.getElementById("ED-view-wrapper").style.display = "block";
                break;
            default:
                break;
        }
    }

    const addNewButton = <button
        className={`button button-${taskState.isAddNew ? 'primary' : 'cancel'}`}
        onClick={showOrHideForm}
    >
        {!taskState.isAddNew ? 'Cancel' : 'Add New'}
    </button>
    const eachDataButtons = (ed) => {
        return (
            <>
                <button
                    name={taskState.isEditing === ed._id ? 'cancel' : 'delete'}
                    className={`button button-${taskState.isEditing === ed._id ? 'cancel' : 'remove'}`}
                    value={ed._id}
                    onClick={handleEachData}
                >
                    {taskState.isEditing === ed._id ? 'Cancel' : 'Delete'}
                </button>
                <button
                    name={taskState.isEditing === ed._id ? 'save' : 'edit'}
                    className={`button button-${taskState.isEditing === ed._id ? 'primary' : 'secondary'}`}
                    value={ed._id}
                    onClick={handleEachData}
                >
                    {taskState.isEditing === ed._id ? 'Save' : 'Edit'}
                </button>
                {taskState.isEditing !== ed._id &&
                <button
                    className={`button button-primary`}
                    value={ed._id}
                    onClick={handleEachData}
                    name='view'
                >
                    View
                </button>
                }
            </>
        )
    }
    const viewDataDiv = <div className="modal-box" id="ED-view-wrapper">
        <div className="AA-view-data" id='ED-view-data'>
            <span className="modal-close" onClick={hideView}>
            &times;
            </span>
            <h2>{taskState.viewData.name}</h2>
            <img src={taskState.viewData.image
                ? `${ADVENTURE_ACT_IMG}${taskState.viewData.image}`
                : require('../../../profile-pic.png')
            } alt='member.png'/>
            <p>{taskState.viewData.detail}</p>
        </div>
    </div>

    return (
        <>
            {viewDataDiv}
            <div className='adventure-activities-wrapper'>
                {addNewButton}
                <Form id='company-ourTeam-form'>
                    <Input
                        id='newN'
                        ref={newInputRef}
                        allowClear
                        name='name'
                        placeholder='Name of the activity...'
                        value={formState.new.name}
                        onChange={handleChange}
                    />
                    <TextArea
                        id='newD'
                        allowClear
                        name='detail'
                        placeholder='Detail of the activity...'
                        onChange={handleChange}
                        value={formState.new.detail}
                    />
                    {formState.new.name &&
                    <button className="button button-primary" onClick={handleAddNew}>
                        Add
                    </button>
                    }
                </Form>
                <div className='advAct-data-wrapper'>
                    {dbData.length ?
                        dbData.map((ot, i) => {
                            const imageUrl = `${ADVENTURE_ACT_IMG}${ot.image}`
                            return (
                                <div className='advAct-eachData' key={i}>
                                    <div className='advAct-eachData-detail-top'>
                                        {eachDataButtons(ot)}
                                        {taskState.isEditing === ot._id ?
                                            <>
                                                <Input
                                                    autoFocus={true}
                                                    name='name'
                                                    placeholder={ot.name}
                                                    value={formState.old.name}
                                                    onChange={handleChange}
                                                />
                                            </>
                                            : <h2>{ot.name}</h2>
                                        }
                                    </div>
                                    <div className='advAct-eachData-image'>
                                        <ImageUploader
                                            url={`${ADVENTURE_ACT}/upload/${ot._id}`}
                                            method={'PUT'}
                                            saveImgUrl={ot.image ? imageUrl : ''}
                                            cropImage={false}
                                        />
                                    </div>
                                    <div className='advAct-eachData-detail'>
                                        {taskState.isEditing === ot._id ?
                                            <TextArea
                                                name='detail'
                                                placeholder={`detail of ${ot.fullName}...`}
                                                value={formState.old.detail}
                                                onChange={handleChange}
                                            />
                                            : <p>{ot.detail}</p>
                                        }
                                    </div>
                                </div>

                            )
                        })
                        : <div className='No-Data'/>}
                </div>
            </div>
        </>
    )
}

export default AdventureActivities;
import React, {useState} from "react";
import {Form, Input} from "antd";
import {EmailValidator} from "../../utility/Validator";
import httpClient from "../../utility/httpClient";
import {USER} from "../../constants/ApiURL.constant";

function AccountSettingsForm({label, savedValue = '', setEditing, refreshData}) {
    const [formText, setFormText] = useState(savedValue);
    const [onTyping, setOnTyping] = useState(false);
    const [errors, setErrors] = useState({});

    function handleChange(e) {
        const {name, value} = e.target;
        setFormText(value);
        if (name === 'emailId') {
            EmailValidator({
                value,
                errors,
                setErrors,
            });
        }
        delete errors[name];
    }

    function validation() {
        let _errors = {};
        if (!formText) _errors[label] = 'The Field cannot be empty.'
        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }

    function handleSubmit(e) {
        e.preventDefault();
        if (validation()) {
            httpClient.put(USER.GET_USER, {body: {[label]: formText}})
                .then(data => {
                    refreshData();
                })
                .catch(err => console.trace('err: ', err));
            setEditing(false);
        }
    }

    const submitButton = <button
        onClick={handleSubmit}
        className={`button button-${Object.keys(errors).length === 0 ? 'primary' : 'disabled'}`}
        {...Object.assign({}, Object.keys(errors).length === 0 ? null : {disabled: true})}>
        Save
    </button>

    return (
        <Form onSubmitCapture={handleSubmit}>
            <Form.Item
                validateStatus={
                    onTyping ? 'validating' :
                        errors[label] ? 'error' : formText && 'success'
                }
                hasFeedback
                help={onTyping ? null : errors[label] || null}
            >
                <Input
                    id={label}
                    name={label}
                    onChange={handleChange}
                    placeholder={label}
                    value={formText}
                    suffix={submitButton}
                    autoFocus={true}
                    onInput={() => {
                        setOnTyping(true);
                        setTimeout(function () {
                            setOnTyping(false)
                        }, 2000);
                    }}
                />
            </Form.Item>
        </Form>
    )
}

export default AccountSettingsForm;
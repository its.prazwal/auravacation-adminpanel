import React, {useState} from "react";
import {Form, Input} from "antd";
import {LockOutlined} from "@ant-design/icons";
import {PWValidator} from "../../utility/Validator";
import httpClient from "../../utility/httpClient";
import {USER} from "../../constants/ApiURL.constant";
import {showError, showSuccess} from "../../utility/Notify";

const initialFormState = {
    prevPassword: '',
    passWord: '',
    confirmPassword: ''
}

function AccountChangePassword({setState}) {
    const [formState, setFormState] = useState(initialFormState);
    const [errors, setErrors] = useState({});
    const [onTyping, setOnTyping] = useState('');
    const [isSubmitting, setIsSubmitting] = useState(false);

    function handleChange(e) {
        const {name, value} = e.target;
        setFormState(prevState => ({...prevState, [name]: value}));
        if (name !== 'prevPassword') {
            PWValidator({
                name,
                passWord: formState.passWord,
                confirmPassword: formState.confirmPassword,
                value,
                errors,
                setErrors
            });
        }
        delete errors[name];
    }

    function validation(){
        let _errors = {};
        if(!formState.prevPassword) _errors.prevPassword = 'Previous password is required.'
        if(!formState.passWord) _errors.passWord = 'New password is required.'
        if(!formState.confirmPassword) _errors.confirmPassword = 'Please re write the password.'
        setErrors(_errors);
        return Object.keys(_errors).length === 0;
    }

    function handleSubmit(e){
        e.preventDefault();
        setIsSubmitting(true);
        if(validation()){
            httpClient.put(USER.CHANGE_PASSWORD,
                {body: {prevPassword: formState.prevPassword, passWord: formState.passWord}})
                .then(data => {
                    showSuccess(data.data);
                    closeForm();
                })
                .catch(err => {
                    console.trace('err: ', err);
                    if(err.response){
                        showError(err.response.data.msg);
                        setErrors({prevPassword: err.response.data.msg});
                        setIsSubmitting(false);
                    }
                });
        }
    }

    function closeForm(){
        setState(false);
        setErrors({});
        setFormState(initialFormState);
        setOnTyping('');
        setIsSubmitting(false);
    }

    return (
        <div className='password-change-wrapper'>
            <div className='password-change-content'>
                {isSubmitting &&
                <div className='password-change-loader'>
                    <div className='loader'/>
                </div>}
                <span className='modal-close' onClick={closeForm}>&times;</span>
                <h3>Change Password</h3>
                <h6>
                    Please input password of 8 to 15 characters containing at least 1 of
                    each: lowercase, uppercase, numeric digit and special character.
                </h6>
                <Form onSubmitCapture={handleSubmit}>
                    <Form.Item
                        validateStatus={
                            onTyping === 'prevPassword' ? 'validating' :
                                errors.prevPassword ? 'error' : null}
                        hasFeedback
                        help={onTyping === 'prevPassword' ? null : errors.prevPassword || null}
                    >
                        <Input.Password
                            name="prevPassword"
                            onChange={handleChange}
                            placeholder="Old Password"
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            value={formState.prevPassword}
                            onInput={() => {
                                setOnTyping('prevPassword');
                                setTimeout(function () {
                                    setOnTyping('')
                                }, 1500)
                            }}
                        />
                    </Form.Item>
                    <Form.Item
                        validateStatus={
                            onTyping === 'passWord' ? 'validating' :
                                errors.passWord ? 'error' : formState.passWord && 'success'}
                        hasFeedback
                        help={onTyping === 'passWord' ? null : errors.passWord || null}
                    >
                        <Input.Password
                            name="passWord"
                            onChange={handleChange}
                            placeholder="New Password"
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            value={formState.passWord}
                            onInput={() => {
                                setOnTyping('passWord');
                                setTimeout(function () {
                                    setOnTyping('')
                                }, 1500)
                            }}
                            {...Object.assign({}, formState.prevPassword ? null : {disabled: true})}
                        />
                    </Form.Item>
                    <Form.Item
                        validateStatus={
                            onTyping === 'confirmPassword' ? 'validating' :
                                errors.confirmPassword ? 'error' : formState.confirmPassword && 'success'}
                        hasFeedback
                        help={onTyping === 'confirmPassword' ? null : errors.confirmPassword || null}
                    >
                        <Input.Password
                            name="confirmPassword"
                            onChange={handleChange}
                            placeholder="Confirm Password"
                            prefix={<LockOutlined className="site-form-item-icon"/>}
                            value={formState.confirmPassword}
                            onInput={() => {
                                setOnTyping('confirmPassword');
                                setTimeout(function () {
                                    setOnTyping('')
                                }, 1500)
                            }}
                            {...Object.assign({},
                                (formState.passWord && formState.prevPassword) ? null : {disabled: true})}
                        />
                    </Form.Item>
                    <button
                        className={`button button-${Object.keys(errors).length > 0 ? 'disabled' : 'primary'}`}
                        type='submit' onClick={handleSubmit}
                        {...Object.assign(
                            {},
                            Object.keys(errors).length > 0 ? { disabled: true } : null
                        )}
                    >
                        Change
                    </button>
                </Form>
            </div>
        </div>
    )
}

export default AccountChangePassword;
import React, {useState} from "react";
import AccountSettingsForm from "./Account.form";

function AccountSettingsShowDetail({label, value, refreshData}) {
    const noSpaceLabel = label.replace(/\s/g, "").replace(/^./, label[0].toLowerCase());
    const [isEditing, setIsEditing] = useState(false);
    return (
        <table>
            <tbody>
            <tr>
                <td>{label}:</td>
                <td>{isEditing ?
                    <AccountSettingsForm label={noSpaceLabel}
                                         savedValue={value}
                                         setEditing={setIsEditing}
                                         refreshData={refreshData}
                    /> : value}</td>
                <td>
                    <button className='account-settings-edit-cancel'
                            onClick={() => setIsEditing(!isEditing)}>{isEditing ? 'Cancel' : 'Edit'}</button>
                </td>
            </tr>
            </tbody>
        </table>

    )
}

export default AccountSettingsShowDetail;
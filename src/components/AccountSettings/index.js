import React, {useEffect, useState} from "react";

import './Account.settings.css';
import {USER} from "../../constants/ApiURL.constant";
import httpClient from "../../utility/httpClient";
import AccountSettingsShowDetail from "./Account.showingDetail";
import AccountChangePassword from "./Account.changePassword";

function AccountSettings() {
    const [user, setUser] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [isChangingPW, setIsChangingPW] = useState(false);
    useEffect(() => {
        setIsLoading(true);
        refreshData();
    }, [])

    function refreshData() {
        httpClient.get(USER.GET_USER, {})
            .then(data => {
                setUser(data.data[0]);
                setIsLoading(false);
            })
            .catch(err => console.trace('err: ', err));
    }

    return (
        <>
            {isLoading ?
                <div className='loader-wrapper'>
                    <div className='loader'/>
                </div>
                :
                <div className='account-settings-wrapper'>
                    {isChangingPW &&
                    <AccountChangePassword setState = {setIsChangingPW} />
                    }
                    <h1>Admin Account</h1>
                    <div className='account-settings-each'>
                        <h2>Personal Details:</h2>
                        <AccountSettingsShowDetail label='Full Name' value={user.fullName} refreshData={refreshData}/>
                        <AccountSettingsShowDetail label='Email Id' value={user.emailId} refreshData={refreshData}/>
                    </div>
                    <div className='account-settings-each'>
                        <h2>Login Details:</h2>
                        <AccountSettingsShowDetail label='User Name' value={user.userName} refreshData={refreshData}/>
                        <h4 onClick={()=>setIsChangingPW(true)}>Change Password</h4>
                    </div>
                </div>
            }
        </>
    )
}

export default AccountSettings;
import React, {useEffect, useState} from "react";
import httpClient from "../../utility/httpClient";
import {BOOKING, CUSTOMER, PACKAGE} from "../../constants/ApiURL.constant";

import './booking.css';
import {showSuccess} from "../../utility/Notify";

function BookingIndex(props) {
    const [bookingData, setBookingData] = useState([]);
    const [packageData, setPackageData] = useState([])
    const [selectedData, setSelectedData] = useState([]);

    useEffect(() => {
        refreshData();
        httpClient.get(PACKAGE.COMMON, {})
            .then(data => setPackageData(data.data))
            .catch(err => console.trace('error fetching packages: ', err));
    }, [])

    function refreshData() {
        httpClient.get(BOOKING, {})
            .then(data => setBookingData(data.data))
            .catch(err => console.trace('err-fetchingData: ', err));
    }

    function selectRow(e) {
        const {value} = e.target;
        let arrayUpdater = [...selectedData];
        if (value === 'selectAll') {
            if (bookingData.length === selectedData.length) {
                arrayUpdater = [];
            } else {
                bookingData.forEach((book) => {
                    if (!selectedData.some(val => val === book._id.toString())) {
                        arrayUpdater.push(book._id)
                    }
                });
            }
        } else {
            if (arrayUpdater.some(val => val === value.toString())) {
                arrayUpdater = arrayUpdater.filter(val => val !== value);
            } else {
                arrayUpdater.push(value);
            }
        }
        setSelectedData(arrayUpdater);
    }

    function handleRemove(id) {
        httpClient.remove(`${BOOKING}/${id}`, {})
            .then(data => {
                refreshData();
                showSuccess('Booking detail moved to trash.')
            })
            .catch(err => console.trace('err: ', err));
    }

    return (
        <div className='booking-wrapper'>
            <table>
                <thead>
                <tr>
                    <th><input type='checkbox' value='selectAll' onClick={selectRow}
                               checked={selectedData.length === bookingData.length}
                    /></th>
                    <th>Contacted</th>
                    <th>Confirmed</th>
                    <th>Customer Detail</th>
                    <th>Booking Detail</th>
                    <th>Start Date</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {bookingData.length > 0 && packageData.length > 0 ?
                    bookingData.map((bd) => {
                        const packageName = packageData.filter(pk => pk._id === bd.packageId)[0].packageName;
                        const _startDate = new Date(bd.startDate).toDateString();
                        return (
                            <tr
                                id={bd._id}
                                key={bd._id}
                                className={`${bd.checked ? 'booking-checked' : 'booking-unchecked'} 
                                ${selectedData.some(val => val === bd._id.toString()) && 'booking-selected'}`}
                            >
                                <td><input type='checkbox' onClick={selectRow} value={bd._id}
                                           checked={selectedData.some(val => val === bd._id)}
                                /></td>
                                <td className={bd.contacted ? 'booking-tick' : 'booking-cross'}
                                    onClick={() => props.history.push(`/Booking/${bd._id}`)}
                                />
                                <td className={bd.confirmed ? 'booking-tick' : 'booking-cross'}
                                    onClick={() => props.history.push(`/Booking/${bd._id}`)}
                                />
                                <td
                                    onClick={() => props.history.push(`/Booking/${bd._id}`)}
                                >{bd.customerDetail.fullName}</td>
                                <td
                                    onClick={() => props.history.push(`/Booking/${bd._id}`)}
                                >{packageName} - <i>{bd.pax} people
                                    {bd.additionalInfo && ' | Additional Info'}
                                    {bd.specialReq && ' | Special Requirement'}
                                </i>
                                </td>
                                <td
                                    onClick={() => props.history.push(`/Booking/${bd._id}`)}
                                >{_startDate}</td>
                                <td><span onClick={() => handleRemove(bd._id)}>
                                    <i className="far fa-trash-alt"/></span>
                                </td>
                            </tr>
                        )
                    }) :
                    <tr>
                        <td colSpan={6}>
                            <div className='No-Data'/>
                        </td>
                    </tr>
                }
                </tbody>
            </table>
        </div>
    )
}

export default BookingIndex;
import React, {useEffect, useState} from "react";
import httpClient from "../../utility/httpClient";
import {BOOKING, PACKAGE} from "../../constants/ApiURL.constant";
import {showSuccess} from "../../utility/Notify";
import {Input} from "antd";

const TextArea = Input.TextArea;


function BookingView(props) {
    const bookingId = props.match.params.id;
    const [bookingData, setBookingData] = useState({});
    const [packageData, setPackageData] = useState({});
    const [isLoading, setIsLoading] = useState(false);
    const [remarks, setRemarks] = useState('');

    useEffect(() => {
        httpClient.put(`${BOOKING}/${bookingId}`, {body: {checked: true}})
            .then(data => showSuccess('Booking checked.'))
            .catch(err => console.trace('err: ', err));
        refreshData();
    }, []);

    function refreshData() {
        setIsLoading(true);
        httpClient.get(`${BOOKING}/${bookingId}`, {})
            .then(bookData => {
                setBookingData(bookData.data);
                setRemarks(bookData.data.adminRemarks);
                httpClient.get(`${PACKAGE.COMMON}/${bookData.data.packageId}`, {})
                    .then(packageData => {
                        setPackageData(packageData.data);
                        setIsLoading(false);
                    })
                    .catch(err => console.trace('Error fetching package: ', err));
            })
            .catch(err => console.trace('Error fetching booking: ', err));
    }

    function changeStatus(e) {
        const {name} = e.target;
        let _body = {[name]: !bookingData[name]};
        if (name === 'confirmed') {
            _body = {..._body, contacted: true};
        }
        httpClient.put(`${BOOKING}/${bookingData._id}`, {body: _body})
            .then(data => {
                refreshData();
                showSuccess(`status for ${name} changed.`)
            })
            .catch(err => console.trace('err: ', err))
    }

    function handleRemarkChange(e) {
        const {name, value} = e.target;
        if (name === 'remarks') {
            setRemarks(value);
        } else {
            e.preventDefault();
            httpClient.put(`${BOOKING}/${bookingData._id}`, {body: {adminRemarks: remarks}})
                .then(data => {
                    refreshData();
                    showSuccess('Remarks Saved.')
                })
                .catch(err => {
                    console.trace('err: ', err)
                });
        }

    }

    return (
        <>
            {isLoading ?
                <div className='loader-wrapper'>
                    <div className='loader'/>
                </div> :
                <div className='booking-component-wrapper'>
                    <div className='booking-comp-header'>
                        <div className='booking-comp-header-button'>
                            <button className='button button-back-icon' onClick={() => props.history.push('/booking')}>
                                <i
                                    className="fas fa-arrow-left"/></button>
                            <button className={`button button-${bookingData.contacted ? 'primary' : 'secondary'}`}
                                    name='contacted' onClick={changeStatus}
                                    {...Object.assign({}, bookingData.confirmed ? {disabled: true} : null)}>
                                {bookingData.contacted ? 'Contacted ✓' : 'Not Contacted ✘'}</button>
                            <button className={`button button-${bookingData.confirmed ? 'primary' : 'secondary'}`}
                                    name='confirmed' onClick={changeStatus}>
                                {bookingData.confirmed ? 'Confirmed ✓' : 'Not Confirmed ✘'}
                            </button>
                        </div>
                        <h1>: : : Booking Detail : : :</h1>
                    </div>
                    <div className='booking-comp-customer'>
                        <h2><b>Customer
                            Detail: </b>{bookingData.customerDetail.fullName} - {bookingData.customerDetail.country} - {bookingData.customerDetail.emailId} - {bookingData.customerDetail.phone}
                        </h2>
                    </div>
                    <div className='booking-comp-package'>
                        <h2><b>Package
                            Detail: </b>{packageData.country} - {packageData.packageName} - {packageData.region || packageData.adventureType}
                        </h2>
                        <div className='booking-comp-package-tripFacts'>
                            {packageData.tripFacts &&
                            <>
                                <div>
                                    <h5>Transportation: </h5>
                                    <h4>
                                        {packageData.tripFacts.transportation.map((tra, i) => {
                                            if (i < packageData.tripFacts.transportation.length - 1)
                                                return `${tra}, `;
                                            else return tra;

                                        })}
                                    </h4>
                                </div>
                                <div>
                                    <h5>Duration: </h5>
                                    <h4>{packageData.tripFacts.duration}</h4>
                                </div>
                                <div>
                                    <h5>Accommodation: </h5>
                                    <h4>
                                        {packageData.tripFacts.accommodation.map((acc, i) => {
                                            if (i < packageData.tripFacts.accommodation.length - 1)
                                                return `${acc}, `;
                                            else return acc;

                                        })}
                                    </h4>
                                </div>
                                <div>
                                    <h5>Meals: </h5>
                                    <h4>
                                        {packageData.tripFacts.meals.map((mea, i) => {
                                            if (i < packageData.tripFacts.meals.length - 1)
                                                return `${mea}, `;
                                            else return mea;

                                        })}
                                    </h4>
                                </div>
                            </>}
                        </div>
                    </div>
                    <div className='booking-comp-booking-detail'>
                        <h2><b>Number of People: </b>{bookingData.pax}<b> - Start
                            Date: </b>{new Date(bookingData.startDate).toDateString()}</h2>
                        <div>
                            <h3>Additional Information:</h3>
                            <p>{bookingData.additionalInfo}</p>
                        </div>
                        <div>
                            <h3>Special Requirement:</h3>
                            <p>{bookingData.specialReq}</p>
                        </div>
                        <div>
                            <h3>Remarks:</h3>
                            <TextArea
                                name='remarks'
                                allowClear={true}
                                value={remarks}
                                onChange={handleRemarkChange}
                                placeholder='Remarks'
                            />
                            <button type='submit'
                                    name='saveRemarks'
                                    className={`button button-${remarks ? 'primary' : 'disabled'}`}
                                    {...Object.assign({}, remarks ? null : {disabled: true})}
                                    onClick={handleRemarkChange}
                            >Save Remarks
                            </button>
                        </div>
                    </div>
                </div>
            }

        </>
    )
}

export default BookingView;
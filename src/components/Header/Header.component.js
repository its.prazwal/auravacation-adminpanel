import React, {useState} from "react";

import "./Header.css";
import { Link, NavLink } from "react-router-dom";
import {COMPANY_NAME} from "../../constants/CompnayDETAIL.constant";
import AccountChangePassword from "../AccountSettings/Account.changePassword";

function Header(props) {
  const [isChangingPW, setIsChangingPW] = useState(false);

  function logOut() {
    const confirm = window.confirm("Are you sure?");
    if (confirm) {
      window.localStorage.clear();
      props.history.push("/Auth/login");
    }
  }

  return (
    <>
      {isChangingPW && <AccountChangePassword setState = {setIsChangingPW} /> }
      <div className="header">
        <div className="header-title">
          <NavLink to='/Dashboard'>{COMPANY_NAME.toUpperCase()}</NavLink>
        </div>
        <div className="header-navBar">
          <NavLink
            to="/Dashboard"
            className="header-links"
            activeClassName="header-links-active"
          >
            Dashboard
          </NavLink>
          <NavLink
            to="/Package"
            className="header-links"
            activeClassName="header-links-active"
          >
            Package
          </NavLink>
          <NavLink
            to="/Options"
            className="header-links"
            activeClassName="header-links-active"
          >
            Options
          </NavLink>
          <NavLink
            to="/Booking"
            className="header-links"
            activeClassName="header-links-active"
          >
            Booking
          </NavLink>
          <NavLink
              to="/Direct-Mail"
              className="header-links"
              activeClassName="header-links-active"
          >
            Direct Mail
          </NavLink>
          <NavLink
              to="/Reviews"
              className="header-links"
              activeClassName="header-links-active"
          >
            Reviews
          </NavLink>
          <NavLink
              to="/Customers"
              className="header-links"
              activeClassName="header-links-active"
          >
            Customers
          </NavLink>
        </div>
        <div className="header-profile">
          <button className="button button-userProfile">
            <i className="far fa-user"/>
          </button>
          <div className="header-profile-dropdown">
            <Link to="/Account-Settings">
              <button className="hpd-links">Account Settings</button>
            </Link>
            <button className="hpd-links" onClick={() => setIsChangingPW(true)}>
              Change Password
            </button>
            <button className="hpd-links" onClick={logOut}>
              Log Out
            </button>
          </div>
        </div>
      </div>
    </>
  );
}

export default Header;

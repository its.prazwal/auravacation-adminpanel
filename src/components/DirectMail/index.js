import React, {useEffect, useState} from "react";
import httpClient from "../../utility/httpClient";
import {DIRECT_MAIL} from "../../constants/ApiURL.constant";

import './DirectMail.css';
import {showSuccess} from "../../utility/Notify";


function DirectMailIndex() {
    const [directMailData, setDirectMailData] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [selectedData, setSelectedData] = useState({});

    function refreshData() {
        setIsLoading(true);
        httpClient.get(DIRECT_MAIL, {})
            .then(direct => {
                setDirectMailData(direct.data);
                setIsLoading(false);
            })
            .catch(err => console.log('err: ', err));
    }

    useEffect(() => {
        refreshData();
        setSelectedData(directMailData[0]);
    }, []);

    function selectMail(dmId) {
        setSelectedData(directMailData.filter(dm => dm._id === dmId)[0]);
    }

    function changeRead(e) {
        httpClient.put(`${DIRECT_MAIL}/${selectedData._id}`,
            {body: {read: !selectedData.read}})
            .then(data => {
                showSuccess(`Message marked as ${selectedData.read ? 'unread' : 'read'}`);
                refreshData();
            })
            .catch(err => console.trace('err: ', err));
    }

    function changeReply(e) {
        httpClient.put(`${DIRECT_MAIL}/${selectedData._id}`,
            {body: {replied: !selectedData.replied, read: true}})
            .then(data => {
                showSuccess(`Message marked as ${selectedData.replied ? 'un-replied' : 'replied'}`);
                refreshData();
            })
            .catch(err => console.trace('err: ', err));
    }

    function moveToTrash(e) {
        httpClient.put(`${DIRECT_MAIL}/${selectedData._id}`
            , {body: {deleted: true}})
            .then(data => {
                showSuccess('Message moved to trash.')
                refreshData();
            })
            .catch(err => console.trace('err: ', err));
    }

    function copyText(e) {
        const {name} = e.target;
        const field = document.getElementById(`direct-mail-${name}`);
        field.select();
        field.setSelectionRange(0, 99999)
        document.execCommand('copy');
        field.blur();
        showSuccess('copied to clipboard.')
    }

    return (
        <>
            {isLoading ?
                <div className='loader-wrapper'>
                    <div className='loader'/>
                </div> : <div className='direct-mail-wrapper'>
                    {directMailData.length !== 0 ?
                        <>
                            <div className='direct-mail-customer'>

                                {directMailData.map(dm => {
                                    return (
                                        <div key={dm._id}
                                             className={`direct-mail-each 
                                     ${selectedData && selectedData._id === dm._id && 'direct-mail-selected'}
                                     ${dm.read && 'direct-mail-read'}
                                     ${dm.replied && 'direct-mail-replied'}
                                     `}
                                             onClick={() => selectMail(dm._id)}>
                                            <h2>{selectedData.customerDetail.fullName} - <i>{new Date().toDateString()}</i></h2>
                                            <p>{dm.message}</p>
                                        </div>)
                                })}
                            </div>
                            <div className='direct-mail-message'>
                                {Object.keys(selectedData).length > 0 ?
                                    <>
                                        <div className='direct-mail-message-header'>
                                            <h2>{selectedData.customerDetail.fullName.toUpperCase()}</h2>
                                            <h3>
                                                <button className='button button-primary'
                                                        name='emailId' onClick={copyText}
                                                >Copy Email
                                                </button>
                                                <input id='direct-mail-emailId' value={selectedData.customerDetail.emailId}/>
                                                <button className='button button-primary'
                                                        name='phone' onClick={copyText}
                                                >Copy Number
                                                </button>
                                                <input id='direct-mail-phone' value={selectedData.customerDetail.phone}/>
                                            </h3>
                                        </div>
                                        <div className='direct-mail-message-actions'>
                                            <button
                                                className={`button button-${selectedData.read ? 'secondary' : 'primary'}`}
                                                onClick={changeRead}
                                            >
                                                Mark as {selectedData.read ? 'unread' : 'read'}
                                            </button>
                                            <button
                                                className={`button button-${selectedData.replied ? 'secondary' : 'primary'}`}
                                                onClick={changeReply}
                                            >
                                                Mark as {selectedData.replied ? 'un-replied' : 'replied'}
                                            </button>
                                            <button
                                                className={`button button-cancel`}
                                                onClick={moveToTrash}
                                            ><i className="far fa-trash-alt"/></button>
                                        </div>
                                        <div className='direct-mail-message-data'>
                                            <p>{selectedData.message}</p>
                                        </div>
                                    </> : <div className='No-Data'><br/>Please select a message head.</div>}
                            </div>
                        </> : <div className='No-Data'/>}
                </div>}
        </>
    )
}

export default DirectMailIndex;
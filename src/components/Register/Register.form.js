import React, {useState} from "react";
import {Input, Tooltip, Form} from 'antd';
import { InfoCircleOutlined, UserOutlined, LockOutlined, MailOutlined, UserAddOutlined } from '@ant-design/icons';

function RegisterForm(props) {
  const { handleSubmit, errors, handleChange, value } = props;
  const [onTyping, setOnTyping] = useState('');
  const submitButton = (
    <button
      type="submit"
      className={`button button-${
        errors ? (Object.keys(errors).length > 0 ? "disabled" : 'primary') : null
      }`}
      {...Object.assign(
        {},
        Object.keys(errors).length > 0 ? { disabled: true } : null
      )}
        onClick={handleSubmit}
    >
      Register
    </button>
  );

  return (
    <Form className="form-wrapper">
        <h6>
            Please input password of 8 to 15 characters containing at least 1 of
            each: lowercase, uppercase, numeric digit and special character.
        </h6>
        <Form.Item
            validateStatus={
                onTyping === 'fullName' ? 'validating' :
                errors.fullName ? 'error' : value.fullName && 'success'
            }
            hasFeedback
            help={onTyping === 'fullName' ? null: errors.fullName || null}
        >
        <Input
          id='error'
          name="fullName"
          onChange={handleChange}
          placeholder="Enter your Name"
          prefix={<UserAddOutlined className="site-form-item-icon" />}
          value={value.fullName}
          autoFocus={true}
          onInput={()=> {
              setOnTyping('fullName');
              setTimeout(function(){
                  setOnTyping('')
              },  2200);
          }}
        />
        </Form.Item>
        <Form.Item
            validateStatus={
                onTyping === 'userName' ? 'validating' :
                errors.userName ? 'error' : value.userName && 'success'}
            hasFeedback
            help={onTyping === 'userName' ? null : errors.userName || null}
        >
      <Input
          name="userName"
          maxLength={12}
          onChange={handleChange}
          placeholder="Enter your username"
          prefix={<UserOutlined className="site-form-item-icon" />}
          suffix={ <Tooltip title="Maximum 12 characters"> <InfoCircleOutlined /> </Tooltip> }
          value={value.userName}
          onInput={()=> {
              setOnTyping('userName');
              setTimeout(function(){
                  setOnTyping('')
              },  2200);
          }}
      />
        </Form.Item>
        <Form.Item
            validateStatus={
                onTyping === 'emailId' ? 'validating' :
                errors.emailId ? 'error' : value.emailId && 'success'}
            hasFeedback
            help={onTyping === 'emailId' ? null : errors.emailId || null}
        >
      <Input
      name="emailId"
      onChange={handleChange}
      placeholder="Enter your Email ID"
      prefix={<MailOutlined className="site-form-item-icon"/>}
      value={value.emailId}
      onInput={()=> {
          setOnTyping('emailId');
          setTimeout(function(){
              setOnTyping('')
          },  2200)
      }}
      />
        </Form.Item>
        <Form.Item
            validateStatus={
                onTyping === 'passWord' ? 'validating' :
                errors.passWord ? 'error' : value.passWord && 'success'}
            hasFeedback
            help={onTyping === 'passWord' ? null: errors.passWord || null}
        >
      <Input.Password
          name="passWord"
          onChange={handleChange}
          placeholder="Password"
          prefix={<LockOutlined className="site-form-item-icon"/>}
          value={value.passWord}
          onInput={()=> {
              setOnTyping('passWord');
              setTimeout(function(){
                  setOnTyping('')
              },  2200)
          }}
      />
        </Form.Item>
        {value.passWord ?
        <Form.Item
            validateStatus={
                onTyping === 'confirmPassword' ? 'validating' :
                errors.confirmPassword ? 'error' : value.confirmPassword && 'success'}
            hasFeedback
            help={ onTyping === 'confirmPassword' ? null : errors.confirmPassword || null}
        >
        <Input.Password
            name="confirmPassword"
            onChange={handleChange}
            placeholder="Confirm Password"
            prefix={<LockOutlined className="site-form-item-icon"/>}
            value={value.confirmPassword}
            onInput={()=> {
                setOnTyping('confirmPassword');
                setTimeout(function(){
                    setOnTyping('')
                },  2200)
            }}
        />
        </Form.Item>
        : null}
      {submitButton}
    </Form>
  );
}

export default RegisterForm;

import React, { useState } from "react";

import httpClient from "./../../utility/httpClient";
import * as notify from "../../utility/Notify";
import {PWValidator, EmailValidator} from '../../utility/Validator';
import {USER} from "../../constants/ApiURL.constant";

import RegisterForm from "./Register.form";

function Register(props) {
  const [regData, setRegData] = useState({
    fullName: '',
    emailId: '',
    userName: '',
    passWord: '',
    confirmPassword: '',
  });
  const [errors, setErrors] = useState({});

  function handleChange(e) {
    const { name, value } = e.target;
    setRegData(prevState => ({...prevState, [name]: value}));
    if(name === 'passWord' || name === 'confirmPassword'){
    PWValidator({
      name,
      passWord: regData.passWord,
      confirmPassword: regData.confirmPassword,
      errors,
      setErrors,
      value,
    });
    }
    if(name === 'emailId'){
      EmailValidator({
        value,
        errors,
        setErrors,
      });
    }
    if(name === 'passWord' && value === ""){
      setRegData(prevState => ({...prevState, confirmPassword: ''}));
      delete errors.confirmPassword;
    }
    delete errors[name];
  }

  function handleSubmit(e) {
    e.preventDefault();
    if (validForm()) {
      httpClient
        .get(USER.REGISTER, {}, false)
        .then((data) => {
          httpClient
            .post(USER.REGISTER, { body: regData }, false)
            .then((data) => {
              props.history.push("/");
              notify.showSuccess("New Admin Added Successfully.");
            })
            .catch((err) => {
              notify.showError("Operation Unsuccessful");
            });
        })
        .catch((err) => {
          setTimeout(() => {
            props.history.push("/");
            notify.showInfo("Please Login to Continue");
          }, 1500);
          notify.showInfo("Admin already existed.");
        });
    }
  }

  function validForm() {
    const _errors = {};
    if (!regData.fullName) _errors.fullName = "Full Name is required.";
    if (!regData.emailId) _errors.emailId = "Email Id is required.";
    if (!regData.userName) _errors.userName = "User Name is required.";
    if (!regData.passWord) _errors.passWord = "Password is required.";
    if (regData.passWord && !regData.confirmPassword)
      _errors.confirmPassword = "Please write the password again";

    setErrors(_errors);
    return Object.keys(_errors).length === 0;
  }

  return (
    <div className="auth-page">
        <h1>Registration</h1>
        <h5>Please fill out all fields correctly.</h5>
        <RegisterForm
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        errors={errors}
        value={regData}
        />
    </div>
  );
}

export default Register;

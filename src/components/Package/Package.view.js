import React from "react";

function PackageView({ viewData, setViewData, handleEdit, handleDisOpt }) {
  function hideView() {
    setViewData(prevState => ({...prevState, id: "", data: {} }));
  }
  document.addEventListener('keydown', (evt) => {
    if(evt.key === 'Escape')
      hideView();
  })
  console.log('checking re exe. in view');

  return (
    <div className="modal-box" id="package-view-wrapper">
      <div className="package-view-data">
        <button
          value={viewData._id}
          type="button"
          className="button button-edit"
          name="edit"
          onClick={handleEdit}
        >
          Edit
        </button>
        <button
          value={viewData._id}
          type="button"
          className={`button button-${
            viewData.displayOption ? "input" : "remove"
          }`}
          name={`${viewData.displayOption}`}
          onClick={handleDisOpt}
        >
          {viewData.displayOption ? "Enabled" : "Disabled"}
        </button>
        <span className="modal-close" onClick={hideView}>
          &times;
        </span>
        <h1>{viewData.packageName}</h1>
        <h4>
          {viewData.country} - {viewData.region}
        </h4>
        <div className="package-view-tripFacts">
          <table>
            <tbody>
              <tr>
                <td>
                  Duration: <b>{viewData.tripFacts.duration || 0} Days</b>
                </td>
                <td>
                  Price: <b>USD {viewData.tripFacts.price || 0}</b>
                </td>
                <td>
                  Altitude: <b>{viewData.tripFacts.altitude || 0} meters</b>
                </td>
                <td>
                  Difficulty: <b>{viewData.tripFacts.difficulty || "None"}</b>
                </td>
              </tr>
              <tr>
                <td colSpan="2">
                  Transportation:{" "}
                  <b>
                    {viewData.tripFacts.transportation.map((tr, i) =>
                      i === 0 ? `${tr}` : ` / ${tr}`
                    ) || "None"}
                  </b>
                </td>
                <td colSpan="2">
                  Accommodation:{" "}
                  <b>
                    {viewData.tripFacts.accommodation.map((ac, i) =>
                      i === 0 ? `${ac}` : ` & ${ac}`
                    ) || "None"}
                  </b>
                </td>
              </tr>
              <tr>
                <td colSpan="2">
                  Meals:{" "}
                  <b>
                    {viewData.tripFacts.meals.map((me, i) =>
                      i === 0 ? `${me}` : ` , ${me}`
                    ) || "None"}
                  </b>
                </td>
                <td colSpan="2">
                  Seasons:{" "}
                  <b>
                    {viewData.tripFacts.seasons.map((se, i) =>
                      i === 0 ? `${se}` : `, ${se}`
                    ) || "None"}
                  </b>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className="package-view-description">
          <p>{viewData.description}</p>
        </div>
        <div className="package-view-highlights">
          <h2>Trip Highlights</h2>
          <ul>
            {viewData.highlights.map((hl, i) => (
              <li key={i}>{hl}</li>
            ))}
          </ul>
        </div>
        <div className="package-view-itinerary-wrapper">
          <h2>Detail Itinerary:</h2>
          {viewData.itinerary.map((iti, i) => {
            return (
              <div className="package-view-itinerary-eachData" key={i}>
                <h3>
                  Day: {iti.day} | {iti.title}
                </h3>
                <p>{iti.detail}</p>
              </div>
            );
          })}
        </div>
        <div className="package-view-costDetails">
          <h2>Cost Details: </h2>
          <h3>Includes</h3>
          <ul>
            {viewData.costDetails.includes.map((cdi, i) => {
              return <li key={i}>{cdi}</li>;
            })}
          </ul>
          <h3>Excludes</h3>
          <ul>
            {viewData.costDetails.excludes.map((cde, i) => {
              return <li key={i}>{cde}</li>;
            })}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default PackageView;

import React, {useState, useEffect} from "react";
import "./Form-Fields.css";
import {Input} from "antd";

export function InputField(props) {
    const {e, propsData, value, _id, placeHolder} = props;
    const {errors, onChange} = propsData;
    const _name = e.replace(/\s/g, "").replace(/^./, e[0].toLowerCase());
    const checkType = _name.toLowerCase().includes("password");

    return (
        <>
            <input
                {...Object.assign({}, props.focus ? {autoFocus: true} : null)}
                id={_id || _name}
                placeholder={errors[_name] ? errors[_name] : placeHolder || e}
                name={_name}
                className={`input-field ${value ? null : "input-field-empty"} ${
                    errors[_name] ? "input-error" : null
                }`}
                onChange={onChange}
                type={checkType ? "password" : "text"}
                value={value}
                {...Object.assign({}, props.disabled ? {disabled: true} : null)}
            />
        </>
    );
}

export function InputMultiple(props) {
    const {e, propsData, selected, arrayData} = props;
    const {errors, onChange} = propsData;
    const _name = e.replace(/\s/g, "").replace(/^./, e[0].toLowerCase());
    return (
        <div
            className={`inputMultiple-wrapper ${
                Object.keys(errors).length > 0 ? "iM-w-error" : "iM-w-noError"
            } `}
        >
            <label>{e}:</label>
            {arrayData.map((ele, i) => {
                return (
                    <input
                        key={i}
                        id={typeof ele === "string" ? ele : ele._id}
                        name={_name}
                        className={`button ${
                            selected.some(
                                (sel) =>
                                    sel ===
                                    (typeof ele === "string"
                                        ? ele
                                        : ele.title || ele.name || ele.statement)
                            )
                                ? "button-primary"
                                : "button-secondary"
                        }  ${errors[_name] ? "input-error" : null}`}
                        onClick={onChange}
                        type="button"
                        value={
                            typeof ele === "string"
                                ? ele
                                : ele.title || ele.name || ele.statement
                        }
                    />
                );
            })}
        </div>
    );
}

export function DBDataTableInput(props) {
    let {
        e,
        propsData,
        DBData,
        changingSt,
        stateData,
        editing,
        onSubmit,
    } = props;
    const [showDBData, setShowDBData] = useState(false);
    const _name = e.replace(/\s/g, "").replace(/^./, e[0].toLowerCase());
    useEffect(() => {
        if (DBData.length > 0) {
            const element = document.getElementById(`${e}-ti-dd-td-dbData`);
            showDBData
                ? (element.style.height = `${DBData.length * 45}px`)
                : (element.style.height = "0px");
        }
    }, [showDBData, DBData, e]);

    return (
        <>
            <div className="tableInput-head">
                {DBData.length > 0 && (
                    <>
                        <div
                            colSpan="4"
                            onClick={() => setShowDBData(!showDBData)}
                            className="tableInput-dropDown-td noselect"
                        >
                            {!showDBData
                                ? "Select from DATABASE store."
                                : "Hide DATABASE store Data."}
                            <i
                                className={`fas fa-angle-down ti-dd-td-i ${
                                    showDBData ? "ti-dd-td-i-rotate" : null
                                }`}
                            />
                        </div>
                        <div id={`${e}-ti-dd-td-dbData`} className="ti-dd-td-dbData">
                            {DBData.map((ele, i) => {
                                const selected = stateData.some(
                                    (sd) => (ele.name || ele.title || ele.statement) === sd
                                );
                                return (
                                    <table>
                                        <tbody>

                                        <tr key={i}>
                                            <td>
                                                <button
                                                    className={`button button-${
                                                        selected ? "disabled" : "input"
                                                    }`}
                                                    value={ele._id}
                                                    name="select"
                                                    onClick={onSubmit}
                                                    {...Object.assign(
                                                        {},
                                                        selected ? {disabled: true} : null
                                                    )}
                                                >
                                                    {selected ? "Selected" : "Select"}
                                                </button>
                                            </td>
                                            <td>{ele.name || ele.statement || ele.title}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                );
                            })}
                        </div>
                    </>
                )}
                <form onSubmit={onSubmit} name="addNew" className="tableInput-form">
                    <Input
                        id='new'
                        placeholder='Statement'
                        value={changingSt.new}
                        name={_name}
                        onChange={propsData.onChange}
                    />
                    <button
                        className={`button button-${changingSt.new ? "input" : "disabled"}`}
                        name="addNew"
                        {...Object.assign({}, changingSt.new ? null : {disabled: true})}
                        type="submit"
                    >
                        Add
                    </button>
                </form>
                <div className='TDND-table-wrapper'>
                    <table className="TDND-table">
                        <thead>
                        <tr>
                            <th> - </th>
                            <th>
                                Statement
                                {editing !== -1 ? <i>editing Statement...</i> : null}
                            </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        {stateData.map((ele, i) => {
                            return (
                                <tr key={i}>
                                    <td>
                                        <button value={i} onClick={onSubmit}
                                                name='removeSt' className='button button-closeIcon'
                                                {...Object.assign({},
                                                    editing !== -1 ? {disabled: true} : null)}>
                                        </button>
                                    </td>
                                    <td>
                                        {editing === i ? (
                                            <form onSubmit={onSubmit} name="saveEdited">
                                                <Input
                                                    id='old'
                                                    placeholder='Editing Statement.'
                                                    value={changingSt.old}
                                                    name={_name}
                                                    onChange={propsData.onChange}
                                                />
                                            </form>
                                        ) : ele}
                                    </td>
                                    <td>
                                        <button
                                            type={editing === i ? "submit" : "button"}
                                            className={`button button-${editing > -1 &&
                                            editing !== i
                                                ? 'disabled-2'
                                                : 'secondary'}`}
                                            name={editing === i? "saveEdited" : "editSt"}
                                            onClick={onSubmit}
                                            value={i}
                                            {...Object.assign({}, editing > -1 &&
                                            editing !== i
                                                ? {disabled: true}
                                                : null)}
                                        >
                                            {editing === i ? "Save" : "Edit"}
                                        </button>
                                        {editing === i &&
                                        <button
                                            className="button button-remove"
                                            name="cancelEdit"
                                            onClick={onSubmit}
                                            type="button"
                                            value={i}
                                        >
                                            Cancel
                                        </button>
                                        }
                                    </td>
                                </tr>
                            );
                        })}
                        </tbody>
                    </table>
                </div>
            </div>

        </>
    );
}

import React, {useState} from "react";

import PackageFormUI from "./PackageForm.UI";

function PackageFormBL(props) {
    const {state, setState, updateDB} = props;
    const {
        changingPackage,
        errors,
        changingArrSt,
        isEditing,
        DBData,
    } = state;
    const [incOrExc, setIncOrExc] = useState("includes");

    function handleTextChange(e) {
        const {name, value} = e.target;
        setState((prevState) => ({
            ...prevState,
            changingPackage: {...prevState.changingPackage, [name]: value},
        }));
    }

    function handleRadioChange(e) {
        const {name, value} = e.target;
        if (name === 'styles') {
            if (value !== 'Trekking')
                setState(prevState => ({
                    ...prevState,
                    changingPackage: {...prevState.changingPackage, region: ''}
                }))
            if (value !== 'Adventure Activities')
                setState(prevState => ({
                    ...prevState,
                    changingPackage: {...prevState.changingPackage, adventureType: ''}
                }))
        }
        setState((prevState) => ({
            ...prevState,
            changingPackage: {...prevState.changingPackage, [name]: value},
        }));

    }

    function handleFactChange(e) {
        const {name, type, value} = e.target;
        let _data;
        switch (type) {
            case "text":
                _data = value;
                break;
            case 'number':
                _data = value;
                if (name === 'duration') {
                    let itinerarySet = [];
                    for (
                        let i = 1;
                        i <= parseInt(value);
                        i++
                    ) {
                        itinerarySet.push({
                            day: i,
                            title: "",
                            detail: "",
                        });
                    }
                    setState((prevState) => ({
                        ...prevState,
                        changingPackage: {
                            ...prevState.changingPackage,
                            itinerary: itinerarySet,
                        },
                    }));
                }
                break;
            case "button":
                if (name === "difficulty") {
                    _data = value;
                } else {
                    let updatedArray = [...changingPackage.tripFacts[name]];
                    if (changingPackage.tripFacts[name].some((fact) => fact === value)) {
                        updatedArray = updatedArray.filter((val) => val !== value);
                    } else {
                        updatedArray.push(value);
                    }
                    _data = updatedArray;
                }
                break;
            default:
                break;
        }
        setState((prevState) => ({
            ...prevState,
            changingPackage: {
                ...prevState.changingPackage,
                tripFacts: {
                    ...prevState.changingPackage.tripFacts,
                    [name]: _data,
                },
            },
        }));
    }

    function handleArrayChange(e) {
        const {name, value, id} = e.target;
        setState((prevState) => ({
            ...prevState,
            changingArrSt: {
                ...prevState.changingArrSt,
                [id]: {...prevState.changingArrSt[id], [name]: value},
            },
        }));
    }

    function handleHighlights(e) {
        const {name, value} = e.target;
        e.preventDefault();
        let arrayUpdater = [...changingPackage.highlights];
        let _data = {
            _CP: changingPackage.highlights,
            _CASn: changingArrSt.new.highlights,
            _CASo: changingArrSt.old.highlights,
            _iE: isEditing.highlights,
        };
        switch (name) {
            case "addNew":
                arrayUpdater.unshift(changingArrSt.new.highlights);
                _data = {..._data, _CP: arrayUpdater, _CASn: ""};
                if (_data._iE !== -1)
                    _data = {
                        ..._data,
                        _CASo: _data._CP.filter((hl, i) => i === parseInt(_data._iE + 1))[0],
                        _iE: _data._iE + 1,
                    }
                break;
            case "editSt":
                _data = {
                    ..._data,
                    _CASo: _data._CP.filter((hl, i) => i === parseInt(value))[0],
                    _iE: parseInt(value),
                };
                break;
            case "saveEdited":
                arrayUpdater.splice(
                    isEditing.highlights,
                    1,
                    changingArrSt.old.highlights
                );
                _data = {..._data, _CP: arrayUpdater, _CASo: "", _iE: -1};
                break;
            case "cancelEdit":
                _data = {..._data, _CASo: "", _iE: -1};
                break;
            case "removeSt":
                arrayUpdater.splice(parseInt(value), 1);
                _data = {..._data, _CP: arrayUpdater};

                break;
            case "select":
                arrayUpdater.unshift(
                    DBData.highlights.filter((hl) => hl._id === value)[0].statement
                );
                _data = {..._data, _CP: arrayUpdater};
                break;
            default:
                break;
        }
        setState((prevState) => ({
            ...prevState,
            changingPackage: {
                ...prevState.changingPackage,
                highlights: _data._CP,
            },
            changingArrSt: {
                ...prevState.changingArrSt,
                new: {
                    ...prevState.changingArrSt.new,
                    highlights: _data._CASn,
                },
                old: {
                    ...prevState.changingArrSt.new,
                    highlights: _data._CASo,
                },
            },
            isEditing: {...prevState.isEditing, highlights: _data._iE},
        }));
    }

    function handleItineraryChange(e) {
        const {name, id, value} = e.target;
        let arrayUpdater = [...changingPackage.itinerary];
        arrayUpdater[parseInt(id - 1)][name] = value;
        setState((prevState) => ({
            ...prevState,
            changingPackage: {
                ...prevState.changingPackage,
                itinerary: arrayUpdater,
            },
        }));
    }

    function handleCostDetails(e) {
        const {name, value} = e.target;
        e.preventDefault();
        let arrayUpdater = [...changingPackage.costDetails[incOrExc]];
        let _data = {
            _CP: changingPackage.costDetails[incOrExc],
            _CASn: changingArrSt.new[incOrExc],
            _CASo: changingArrSt.old[incOrExc],
            _iE: isEditing[incOrExc],
        };
        switch (name) {
            case "addNew":
                arrayUpdater.unshift(changingArrSt.new[incOrExc]);
                _data = {..._data, _CP: arrayUpdater, _CASn: ""};
                if(_data._iE !== -1){
                    _data = {
                        ..._data,
                        _CASo: _data._CP.filter((hl, i) => i === parseInt(_data._iE + 1))[0],
                        _iE: _data._iE + 1,
                    };
                }
                break;
            case "editSt":
                _data = {
                    ..._data,
                    _CASo: _data._CP.filter((hl, i) => i === parseInt(value))[0],
                    _iE: parseInt(value),
                };
                break;
            case "saveEdited":
                arrayUpdater.splice(
                    isEditing[incOrExc],
                    1,
                    changingArrSt.old[incOrExc]
                );
                _data = {..._data, _CP: arrayUpdater, _CASo: "", _iE: -1};
                break;
            case "cancelEdit":
                _data = {..._data, _CASo: "", _iE: -1};
                break;
            case "removeSt":
                arrayUpdater.splice(parseInt(value), 1);
                _data = {..._data, _CP: arrayUpdater};
                break;
            case "select":
                arrayUpdater.unshift(
                    DBData.costDetails[incOrExc].filter((hl) => hl._id === value)[0]
                        .statement
                );
                _data = {..._data, _CP: arrayUpdater};
                break;

            default:
                break;
        }
        setState((prevState) => ({
            ...prevState,
            changingPackage: {
                ...prevState.changingPackage,
                costDetails: {
                    ...prevState.changingPackage.costDetails,
                    [incOrExc]: _data._CP,
                },
            },
            changingArrSt: {
                ...prevState.changingArrSt,
                new: {
                    ...prevState.changingArrSt.new,
                    [incOrExc]: _data._CASn,
                },
                old: {
                    ...prevState.changingArrSt.old,
                    [incOrExc]: _data._CASo,
                },
            },
            isEditing: {...prevState.isEditing, [incOrExc]: _data._iE},
        }));
    }

    return (
        <PackageFormUI
            handleArrayChange={handleArrayChange}
            handleFactChange={handleFactChange}
            handleHighlights={handleHighlights}
            handleRadioChange={handleRadioChange}
            handleTextChange={handleTextChange}
            handleItineraryChange={handleItineraryChange}
            handleCostDetails={handleCostDetails}
            changingArrSt={changingArrSt}
            changingPackage={changingPackage}
            errors={errors}
            isEditing={isEditing}
            DBData={DBData}
            incOrExc={incOrExc}
            setIncOrExc={setIncOrExc}
            updateDB={updateDB}
        />
    );
}

export default PackageFormBL;

import React, { useState, useEffect } from "react";
import httpClient from "./../../utility/httpClient";

import SideBar from "./../Sidebar/Sidebar.component";
import PackageTable from "./Package.table";
import PackageFormST from "./PackageForm.ST";
import "./ManagePackage.css";

const initialState = {
  isLoaded: false,
  packageStyles: [],
  packageList: [],
};

function ManagePackage(props) {
  const [state, setState] = useState(initialState);
  var _which = props.match.params.which;

  useEffect(() => {
    httpClient
      .get("/tripFacts/styles", {})
      .then((data) => {
        const _packageStyles = data.data.map((sty) => sty.name);
        setState((prevState) => ({
          ...prevState,
          packageStyles: _packageStyles,
          isLoaded: true,
        }));   
        refreshData();
      })
      .catch((err) => console.trace("err: ", err));
  }, [_which]);

  function refreshData() {
    httpClient
      .get("/package/search", {
        params: { style: _which.replace(/-/, " ") },
      })
      .then((data) => {
        setState((prevState) => ({ ...prevState, packageList: data.data }));
      })
      .catch((err) => {
        console.trace("err: ", err);
      });
  }

  function handleEdit(e) {
    const { value } = e.target;
    console.log("handleEdit: ", value);
  }

  function handleDisOpt(e) {
    const { name, value } = e.target;
    httpClient
      .put(`/package/${value}`, {
        body: { displayOption: name !== "false" },
      })
      .then((data) => {
        refreshData();
        console.log("success");
      })
      .catch((err) => {
        console.log("error");
      });
  }

  return (
    <>
      {!state.isLoaded ? (
        <div className="loader-wrapper">
          <div className="loader"/>
        </div>
      ) : (
        <>
          <SideBar
            mainNav="Package"
            navs={["Add New", ...state.packageStyles]}
          />
          <div className="main-content">
            {_which === "Add-New" ? (
              <PackageFormST history={props.history} />
            ) : (
              <PackageTable
                packageList={state.packageList}
                handleDisOpt={handleDisOpt}
                handleEdit={handleEdit}
              />
            )}
          </div>
        </>
      )}
    </>
  );
}

export default ManagePackage;

import React, {useState, useEffect} from "react";
import {ImageUploader} from "../Common/Uploader";
import {PACKAGE} from "../../constants/ApiURL.constant";
import {PACKAGE_IMG} from "../../constants/IMGURL.constant";
import PackageView from "./Package.view";

function PackageTable({packageList, handleEdit, handleDisOpt}) {
    const [viewData, setViewData] = useState({
        id: "",
        data: {},
        image: {name: '', id: ''},
    });

    useEffect(() => {
        if (viewData.id) {
            console.log("got id");
            console.log(
                "pack: ",
                packageList.filter((pl) => pl._id === viewData.id)[0]
            );
            setViewData((prevState) => ({
                ...prevState,
                data: packageList.filter((pl) => pl._id === viewData.id)[0],
            }));
        }
    }, [packageList, viewData.id]);
    if (viewData.image.id) {
        document.addEventListener(
            'keydown',
            (evt) => evt.key === 'Escape' && handleShowHideImage('hide'),
            false)
    }

    function handleShowHideImage(task, e) {
        if (task === 'show') {
            setViewData(prevState => ({...prevState, image: {name: e.image, id: e._id}}))
            document.getElementById('package-image-wrapper').style.display = 'block'
        } else {
            document.getElementById('package-image-wrapper').style.display = 'none'
            setViewData(prevState => ({...prevState, image: {name: '', id: ''}}))
        }
        return null;
    }

    return (
        <>
            {Object.keys(viewData.data).length > 0 && (
                <PackageView
                    viewData={viewData.data}
                    setViewData={setViewData}
                    handleEdit={handleEdit}
                    handleDisOpt={handleDisOpt}
                />
            )}
            <div className='modal-box' id='package-image-wrapper'>
                <div className='package-image-content'>
                    <span className='package-image-edit-text'>Click to edit picture...</span>
                    <span className='modal-close' onClick={() => handleShowHideImage('hide')}>&times;</span>
                    {viewData.image.id &&
                    <ImageUploader
                        url={`${PACKAGE.UPLOAD}/${viewData.image.id}`}
                        {...Object.assign({}, viewData.image.name ?
                            {saveImgUrl: `${PACKAGE_IMG}/${viewData.image.name}`} : null
                        )}
                        method={'PUT'}
                        cropImage={false}
                    />
                    }
                </div>
            </div>
            <div className="table-wrapper">
                <table className="packageList-table">
                    <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Image</th>
                        <th>Package Detail</th>
                        <th>View</th>
                        <th>Edit</th>
                        <th>Display</th>
                    </tr>
                    </thead>
                    <tbody>
                    {packageList.length > 0 ?
                        packageList.map((e, i) => {
                            return (
                                <tr
                                    key={i}
                                    {...Object.assign(
                                        {},
                                        e.displayOption
                                            ? null
                                            : {className: "display-option-disabled"}
                                    )}
                                >
                                    <td>{i + 1}</td>
                                    <td>
                                        {e.image ?
                                            <img src={`${PACKAGE_IMG}/${e.image}`} alt='pck.img'
                                                 onClick={() => handleShowHideImage('show', e)}/>
                                            :
                                            <div className='upload-image'
                                                 onClick={() => handleShowHideImage('show', e)}/>
                                        }
                                    </td>
                                    <td>
                                        <h3>{e.packageName}</h3>
                                        <p>USD {e.tripFacts.price} | {e.tripFacts.duration} Days</p>
                                    </td>
                                    <td>
                                        <button
                                            value={e._id}
                                            type="button"
                                            className="button button-primary"
                                            name="view"
                                            onClick={() => setViewData(prevState => ({
                                                ...prevState,
                                                id: e._id,
                                                data: e
                                            }))}
                                        >
                                            View
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            value={e._id}
                                            type="button"
                                            className="button button-secondary"
                                            name="edit"
                                            onClick={handleEdit}
                                        >
                                            Edit
                                        </button>
                                    </td>
                                    <td>
                                        <button
                                            type="button"
                                            className={`button button-${
                                                e.displayOption ? "secondary-green" : "remove"
                                            } `}
                                            onClick={handleDisOpt}
                                            value={e._id}
                                            name={`${!e.displayOption}`}
                                        >
                                            {e.displayOption ? "Enabled" : "Disabled"}
                                        </button>
                                    </td>
                                </tr>
                            );
                        }) :
                        <tr>
                            <td colSpan={5}>
                                <div className='No-Data'/>
                            </td>
                        </tr>
                    }
                    </tbody>
                </table>
            </div>
        </>
    );
}

export default PackageTable;

import React, {useState, useEffect} from "react";

import PackageFormBL from "./PackageForm.BL";
import httpClient from "./../../utility/httpClient";
import * as notify from "./../../utility/Notify";
import {PACKAGE, ADVENTURE_ACT} from "../../constants/ApiURL.constant";

const initialState = {
    changingPackage: {
        adventureType: '',
        costDetails: {
            includes: [],
            excludes: [],
        },
        country: "",
        description: "",
        displayOption: true,
        highlights: [],
        image: '',
        itinerary: [],
        packageName: "",
        region: "",
        reviewsList: [],
        style: "",
        tripFacts: {
            accommodation: [],
            altitude: "",
            difficulty: "",
            duration: "",
            grade: "",
            meals: [],
            price: "",
            seasons: [],
            transportation: [],
        },
        usefulInfo: [],
    },
    changingArrSt: {
        new: {
            excludes: "",
            highlights: "",
            includes: "",
        },
        old: {
            excludes: "",
            highlights: "",
            includes: "",
        },
    },
    isEditing: {
        excludes: -1,
        highlights: -1,
        includes: -1,
    },
    DBData: {
        accommodation: [],
        adventureType: [],
        costDetails: {
            includes: [],
            excludes: [],
        },
        country: [],
        highlights: [],
        region: [],
        styles: [],
        transportation: [],
    },
    errors: {},
};

function PackageFormST(props) {
    const [state, setState] = useState(initialState);
    const [dataNotFound, setDataNotFound] = useState([]);
    useEffect(() => {
        getCostDetails();
        getTripFacts();
        getAdventureType();
    }, []);


    function getTripFacts() {
        ['accommodation', 'country', 'highlights', 'region', 'styles', 'transportation'].forEach((eK) => {
            httpClient
                .get(`/tripFacts/${eK}`, {})
                .then((data) => {
                    if (data.data.length === 0) {
                        setDataNotFound(oldArray => [...oldArray, eK])
                    }
                    setState((prevState) => ({
                        ...prevState,
                        DBData: {
                            ...prevState.DBData,
                            [eK]: data.data,
                        },
                    }));
                })
                .catch((err) => {
                    console.trace("err: ", err);
                });
            return null
        })
    }

    function getCostDetails() {
        ['includes', 'excludes'].map((cD) => {
            httpClient
                .get(`/costDetails/${cD}`, {})
                .then((data) => {
                    if (data.data.length === 0) {
                        setDataNotFound(oldArray => [...oldArray, cD])
                    }
                    setState((prevState) => ({
                        ...prevState,
                        DBData: {
                            ...prevState.DBData,
                            costDetails: {
                                ...prevState.DBData.costDetails,
                                [cD]: data.data,
                            },
                        },
                    }));
                })
                .catch((err) => {
                    console.trace("err: ", err);
                });
            return null;
        })
    }

    function getAdventureType() {
        httpClient.get(ADVENTURE_ACT, {})
            .then((data) => {
                if (data.data.length === 0) {
                    setDataNotFound(oldArray => [...oldArray, 'Adventure Activities'])
                }
                setState((prevState) => ({
                    ...prevState,
                    DBData: {
                        ...prevState.DBData,
                        adventureType: data.data,
                    },
                }));
            })
            .catch((err) => {
                console.trace("err: ", err);
            });
    }

    function updateDB(e) {
        e.preventDefault();
        const {name} = e.target;
        if (name === "addNewPackage") {
            httpClient
                .post(PACKAGE.COMMON, {body: state.changingPackage})
                .then((data) => {
                    notify.showSuccess(data.data);
                    props.history.push(`/Package/${state.changingPackage.style}`);
                    setState(initialState);
                })
                .catch((err) => {
                    console.trace("err: ", err);
                });
        }
    }

    return (
        <>
            {dataNotFound.length === 0
                ? <PackageFormBL state={state} setState={setState} updateDB={updateDB}/>
                : <div className='addPackage-allData-notfound'>
                    <h2>Please make sure you have insert all the following data:</h2>
                    {dataNotFound.map((e, i) => {
                        return (
                            <h4 key={i}>{e}</h4>
                        )
                    })}
                    <h2>from options tab.</h2>
                </div>
            }
        </>
    );
}

export default PackageFormST;

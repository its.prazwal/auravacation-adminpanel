import React, {useState} from "react";
import {Form, Input} from "antd";

import {
    InputMultiple,
    DBDataTableInput,
} from "./Form-fields/FormFields";

function PackageFormUI({
                           DBData,
                           handleArrayChange,
                           handleFactChange,
                           handleHighlights,
                           handleRadioChange,
                           handleTextChange,
                           handleItineraryChange,
                           handleCostDetails,
                           changingArrSt,
                           changingPackage,
                           errors,
                           isEditing,
                           incOrExc,
                           setIncOrExc,
                           updateDB,
                       }) {
    const [showItinerary, setShowItinerary] = useState(true);

    function showOrHideIti(e) {
        setShowItinerary(!showItinerary);
        const _length = changingPackage.itinerary.length * 190;
        if (showItinerary) {
            document.getElementById("itinerary-wrapper").style.height = "0px";
        } else {
            document.getElementById(
                "itinerary-wrapper"
            ).style.height = `${_length}px`;
        }
    }

    return (
        <div className="package-form-wrapper">
            <h1>Initial Info:</h1>
            <Form className="package-form" onSubmitCapture={updateDB} name="addNewPackage">
                <InputMultiple
                    e="Country"
                    arrayData={DBData.country}
                    propsData={{onChange: handleRadioChange, errors: errors}}
                    selected={[changingPackage.country]}
                />
                <InputMultiple
                    e="Style"
                    arrayData={DBData.styles}
                    propsData={{onChange: handleRadioChange, errors: errors}}
                    selected={[changingPackage.style]}
                />
                {changingPackage.style === "Trekking" ? (
                    <InputMultiple
                        e="Region"
                        arrayData={DBData.region}
                        propsData={{onChange: handleRadioChange, errors: errors}}
                        selected={[changingPackage.region]}
                    />
                ) : null}
                {changingPackage.style === "Adventure Activities" ? (
                    <InputMultiple
                        e="Adventure Type"
                        arrayData={DBData.adventureType}
                        propsData={{onChange: handleRadioChange, errors: errors}}
                        selected={[changingPackage.adventureType]}
                    />
                ) : null}
                <div className='package-form-packageName'>
                    <label htmlFor='packageName'>Package Name:</label>
                    <Input
                        id='packageName'
                        name="packageName"
                        placeholder='Package Name'
                        allowClear={true}
                        onChange={handleTextChange}
                        value={changingPackage.packageName}
                    />
                </div>
                <h1>Trip Facts:</h1>
                <div className='package-form-tripFacts'>
                    <Input
                        id='duration'
                        name="duration"
                        placeholder='Duration'
                        onChange={handleFactChange}
                        value={changingPackage.tripFacts.duration}
                        suffix='DAYS'
                        type='number'
                    />
                    <Input
                        id='price'
                        name="price"
                        placeholder='Price'
                        onChange={handleFactChange}
                        value={changingPackage.tripFacts.price}
                        suffix='USD'
                        type='number'
                    />
                    <Input
                        id='altitude'
                        name="altitude"
                        placeholder='Altitude'
                        onChange={handleFactChange}
                        value={changingPackage.tripFacts.altitude}
                        suffix='METERS'
                        type='number'
                    />
                </div>
                <InputMultiple
                    e="Difficulty"
                    arrayData={["Easy", "Medium", "Hard", "Extreme"]}
                    propsData={{onChange: handleFactChange, errors: errors}}
                    selected={[changingPackage.tripFacts.difficulty]}
                />
                <InputMultiple
                    e="Transportation"
                    arrayData={DBData.transportation}
                    propsData={{onChange: handleFactChange, errors: errors}}
                    selected={changingPackage.tripFacts.transportation}
                />
                <InputMultiple
                    e="Accommodation"
                    arrayData={DBData.accommodation}
                    propsData={{onChange: handleFactChange, errors: errors}}
                    selected={changingPackage.tripFacts.accommodation}
                />
                <InputMultiple
                    e="Meals"
                    arrayData={["Breakfast", "Lunch", "Dinner"]}
                    propsData={{onChange: handleFactChange, errors: errors}}
                    selected={changingPackage.tripFacts.meals}
                />
                <InputMultiple
                    e="Seasons"
                    arrayData={[
                        "Jan",
                        "Feb",
                        "Mar",
                        "Apr",
                        "May",
                        "Jun",
                        "Jul",
                        "Aug",
                        "Sept",
                        "Oct",
                        "Nov",
                        "Dec",
                    ]}
                    propsData={{onChange: handleFactChange, errors: errors}}
                    selected={changingPackage.tripFacts.seasons}
                />
                <div className='package-form-description'>
                <h1>Package Description: </h1>
                <Input.TextArea
                    name='description'
                    placeholder='Description'
                    allowClear={true}
                    onChange={handleTextChange}
                    value={changingPackage.description}
                />
                </div>
                <h1>highlights: </h1>
                <DBDataTableInput
                    e="highlights"
                    propsData={{onChange: handleArrayChange, errors: errors}}
                    DBData={DBData.highlights}
                    changingSt={{
                        new: changingArrSt.new.highlights,
                        old: changingArrSt.old.highlights,
                    }}
                    stateData={changingPackage.highlights}
                    editing={isEditing.highlights}
                    onSubmit={handleHighlights}
                />
                <h1>Itinerary: </h1>
                {changingPackage.tripFacts.duration ? (
                    <div
                        onClick={showOrHideIti}
                        className="tableInput-dropDown-td noselect"
                    >
                        {!showItinerary ? "Show Itinerary List." : "Hide Itinerary List."}
                        <i className="fas fa-angle-up"/>
                    </div>
                ) : (
                    <p className="information-needed">
                        Please input <i>duration</i> of the trip to input itinerary.
                    </p>
                )}
                <div className="itinerary-wrapper" id="itinerary-wrapper">
                    {changingPackage.itinerary.map((iti, i) => {
                        return (
                            <div className="itinerary-eachData" key={i}>
                                <i>Day: {iti.day}</i>
                                <Input
                                    id={i+1}
                                    name="title"
                                    placeholder={`Title of day ${iti.day}`}
                                    onChange={handleItineraryChange}
                                    value={changingPackage.itinerary[i].title}
                                    allowClear={true}
                                />
                                <Input.TextArea
                                name='detail'
                                placeholder={`Detail of day ${iti.day}`}
                                allowClear={true}
                                onChange={handleItineraryChange}
                                value={changingPackage.itinerary[i].detail}
                                id={i+1}
                            />
                            </div>
                        );
                    })}
                </div>
                <h1>Cost Details: </h1>
                <div className="mP-cD-h">
                    {["includes", "excludes"].map((e, i) => {
                        return (
                            <button
                                key={i}
                                type="button"
                                className={`mP-cD-h-btn ${
                                    incOrExc === e ? "mP-cD-h-btn-slt" : "mP-cD-h-btn-noslt"
                                }`}
                                onClick={() => setIncOrExc(e)}
                            >
                                Cost {e}
                            </button>
                        );
                    })}
                </div>
                    <DBDataTableInput
                        e={incOrExc}
                        propsData={{onChange: handleArrayChange, errors: errors}}
                        DBData={DBData.costDetails[incOrExc]}
                        changingSt={{
                            new: changingArrSt.new[incOrExc],
                            old: changingArrSt.old[incOrExc],
                        }}
                        stateData={changingPackage.costDetails[incOrExc]}
                        editing={isEditing[incOrExc]}
                        onSubmit={handleCostDetails}
                    />
                <input
                    type="Submit"
                    className="button button-primary"
                    value="Submit"
                    onClick={updateDB}
                    name='addNewPackage'
                />
            </Form>
        </div>
    );
}

export default PackageFormUI;

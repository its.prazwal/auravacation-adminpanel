export const COMPANY = {
    ABOUT_US: '/company/aboutUs',
    LEGAL_DOCS: '/company/legalDocs',
    OUR_TEAM: '/company/ourTeam',
    PRIVACY_POLICY: '/company/privacyPolicy',
    WHY_COMPANY: '/company/whyCompany',
    CONTACT_US: '/company/contactUs',
};
export const COST_DETAILS = {
    EXCLUDES: '/costDetails/excludes',
    INCLUDES: '/costDetails/includes',
}
export const LOCATION = {
    COUNTRY: '/tripFacts/country',
    PLACES: '/tripFacts/places',
    REGION: '/tripFacts/region',
}
export const TRIP_FACTS = {
    ACCOMMODATION: '/tripFacts/accommodation',
    HIGHLIGHTS: '/tripFacts/highlights',
    STYLES: '/tripFacts/styles',
    TRANSPORTATION: '/tripFacts/transportation',
};
export const USER = {
    GET_USER: '/user',
    REGISTER: '/user/register',
    LOGIN: '/user/login',
    FORGOT_PASSWORD: '/user/forgot-password',
    VERIFY_PASSWORD: '/user/verify-password',
    RESET_PASSWORD: '/user/reset-password',
    CHANGE_PASSWORD: '/user/change-password',
}

export const ADVENTURE_ACT = '/adventureAct';
export const BOOKING = '/booking';
export const CUSTOMER = '/customers';
export const DIRECT_MAIL = '/directMail';
export const PACKAGE = {
    COMMON: '/package',
    SEARCH: '/package/search',
    UPLOAD: '/package/upload',
};
export const REVIEWS = '/reviews';
export const SUBSCRIBED = '/subscribed';
export const USEFUL_INFO = '/tripFacts/usefulInfo';
export const HOMEPAGE_API = '/homepage';



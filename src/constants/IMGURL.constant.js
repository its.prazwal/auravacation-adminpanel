export const OUR_TEAM_IMG = `${process.env.REACT_APP_IMAGE_URL}/ourTeamIMG/`;
export const LEGAL_DOCS_IMG = `${process.env.REACT_APP_IMAGE_URL}/legalDocsIMG/`;
export const PACKAGE_IMG = `${process.env.REACT_APP_IMAGE_URL}/packageIMG/`;
export const ADVENTURE_ACT_IMG = `${process.env.REACT_APP_IMAGE_URL}/advActIMG/`;
export const DEFAULT = {
    TREK_IMG: `${process.env.REACT_APP_IMAGE_URL}/defaultIMG/TREKKING.png`,
    TOUR_IMG: `${process.env.REACT_APP_IMAGE_URL}/defaultIMG/TOUR.jpg`,
};

